# python library imports.
from urllib.request import urlopen
from urllib.error import URLError
from ssl import SSLContext, PROTOCOL_SSLv23, CERT_NONE

# local imports
from .mis_html_parsers import MisProgrammeParser, MisModuleParser
from timetables.models import Module, Programme, BatchUpdateStatus, TakenBy

BASE_URL = "https://www.mis.nuigalway.ie/regexam/cmrs_module_detail.asp?ay="
PROGRAMME_PARAM = "&instance_code="
MODULE_PARAM = "&module="


# call MIS to find degree programme information for a given year
def get_programme_information(year, programme_id):
    # open the MIS page for this degree programme
    try:
        response = urlopen(BASE_URL + str(year) + PROGRAMME_PARAM +
                           programme_id, context=set_context())
    except URLError as e:
        # failure to connect
        return BatchUpdateStatus(status=e.reason)
    except RuntimeError as e:
        return BatchUpdateStatus(status=str(e))

    # feed the html response to the programme parser
    html = response.read()
    parser = MisProgrammeParser()
    parser.feed(html.decode('latin_1'))

    try:
        # get the programme information gathered by the parser
        information = parser.get_information(programme_id)
    except RuntimeError as e:
        # programme doesn't exist
        return BatchUpdateStatus(status=str(e))

    # save to database if the programme doesn't already exist
    if (information['programme'] not in
            Programme.objects.filter(code=programme_id)):
        information['programme'].save()
    else:
        update_programme(Programme.objects.get(code=programme_id),
                         information['programme'])

    for m in information['modules']:
        # get module name
        get_module_information(year, m)

        # check if module already exists and update it if it does
        if m in Module.objects.filter(code=m.code):
            update_module(Module.objects.filter(code=m.code).first(), m)
        else:
            m.save()

    for t in information['taken_by']:
        for m in information['modules']:
            if t['module'] == m.code:
                taken_by = TakenBy(programme=information['programme'],
                                   module=m,
                                   is_core=t['is_core'])

                if (TakenBy.objects.filter(programme=taken_by.programme)
                        .filter(module=taken_by.module)):
                    update_taken_by(
                        TakenBy.objects.filter(programme=taken_by.programme)
                        .filter(module=taken_by.module).first(), taken_by)
                else:
                    taken_by.save()

    # check for outdated taken_by rows
    for t in TakenBy.objects.filter(programme=information['programme']):
        if t.module not in information['modules']:
            t.delete()

    return BatchUpdateStatus(status="Success")


# call MIS to find module information for a given year
def get_module_information(year, module):
    try:
        response = urlopen(BASE_URL + str(year) + MODULE_PARAM + module.code,
                           context=set_context())

        html = response.read()
    except URLError as e:
        # failure to connect
        return BatchUpdateStatus(status=e.reason)
    except RuntimeError as e:
        return BatchUpdateStatus(status=str(e))

    parser = MisModuleParser()
    parser.feed(html.decode('latin_1'))

    information = parser.get_information(module)


# check if we need to update information for a module that already exists in
# the database
def update_module(old, new):
    if old.code != new.code:
        old.code = new.code

    if old.ects != new.ects:
        old.ects = new.ects

    if old.name != new.name:
        old.name = new.name

    if old.semester != new.semester:
        old.semester = new.semester

    old.save()


def update_programme(old, new):
    if old.name != new.name:
        old.name = new.name

    old.save()


def update_taken_by(old, new):
    if old.is_core != new.is_core:
        old.is_core = new.is_core

    old.save()


# create an SSL context that ignores the fact that MIS has an invalid security
# certificate
def set_context():
    context = SSLContext(PROTOCOL_SSLv23)
    context.verify_mode = CERT_NONE

    return context
