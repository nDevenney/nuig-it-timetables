# python imports
from html.parser import HTMLParser

# local imports
from timetables.models import Module, Programme, TakenBy


class MisProgrammeParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.info = []

    def handle_data(self, data):
        data = data.strip()  # remove whitespace

        if data.strip() != '':
            self.info.append(data)

    def get_information(self, programme_id):
        if 'Does not exist/not active for this academic session' in self.info:
            raise RuntimeError('Programme does not exist')

        if ('The service you are attempting to use is currently unavailable' in
                self.info[0]):
            raise RuntimeError('MIS service currently unavailable')

        structure_levels = 0
        high_level_module_code = ''
        modules = []
        taken_by = []

        for idx, val in enumerate(self.info):
            if val == 'Structure levels':
                structure_levels = int(self.info[idx+1])
                break

        for idx, val in enumerate(self.info):
            if programme_id in val and 'Module' not in val:
                programme_name = val
                break

        if structure_levels == 1:
            for idx, val in enumerate(self.info):
                if (val.__len__() > 3 and 'EX' not in val and
                        ((val[0].isupper() and val[1].isupper() and
                            val[2].isdigit()) or
                            (val[2].isupper() and val[3].isdigit()))):
                    try:
                        if self.info[idx+3] == 'Semester 1':
                            semester = "1"
                        elif self.info[idx+3] == 'Semester 2':
                            semester = "2"
                        elif self.info[idx+3] == 'Semester 1 and Semester 2':
                            semester = "All year"
                        else:
                            semester = "N/A"

                        module = Module(code=val.strip(),
                                        ects=int(self.info[idx+1].strip()),
                                        semester=semester)
                    except ValueError:
                        continue

                    modules.append(module)

                    taken_by.append({
                        'module': module.code,
                        'is_core': (self.info[idx+2].strip() == 'Core')
                    })
        elif structure_levels == 2:
            for idx, val in enumerate(self.info):
                if val.startswith('CT'):
                    high_level_module_code_idx = idx
                    break

            idx = high_level_module_code_idx + 5

            while(self.info[idx].startswith('CT')):
                if self.info[idx+3] == 'Semester 1':
                    semester = "1"
                elif self.info[idx+3] == 'Semester 2':
                    semester = "2"
                elif self.info[idx+3] == 'Semester 1 and Semester 2':
                    semester = "All year"
                else:
                    semester = "N/A"

                module = Module(code=self.info[idx].strip(),
                                ects=int(self.info[idx+1].strip()),
                                semester=semester)

                modules.append(module)

                taken_by.append({
                    'module': module.code,
                    'is_core': (self.info[idx+2].strip() == 'Core')
                })

                idx += 6

        return {
            'programme': Programme(code=programme_id, name=programme_name),
            'modules': modules,
            'taken_by': taken_by
        }


class MisModuleParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.info = []

    def handle_data(self, data):
        data = data.strip()  # remove whitespace

        if data.strip() != '':
            self.info.append(data)

    def get_information(self, module):
        module.name = self.info[self.info.index(module.code + ":") + 1]
