from django.test import TestCase
from django.utils import timezone

from timetables.models import (
    ProgrammeTimetable,
    VenueTimetable,
    TimetableEntry,
    Programme,
    Venue,
    Module,
    TakenBy,
    Lecturer,
    TaughtBy,
    TimetableEntry,
    EntryVenue,
    EntryLecturer,
)


class ModelsTestCase(TestCase):
    def setUp(self):
        create_test_data()

    def test_programme_timetable_entries(self):
        programme = Programme.objects.get(code='1BCT1')
        timetable = ProgrammeTimetable.objects.get(programme=programme)

        entries = timetable.entries()

        self.assertEqual(entries.__len__(), 2)

        self.assertEqual(entries[0].day, 1)
        self.assertEqual(entries[0].time, 9)

        self.assertEqual(entries[1].day, 1)
        self.assertEqual(entries[1].time, 15)

    def test_venue_timetable_entries(self):
        venue = Venue.objects.get(code='IT102')
        timetable = VenueTimetable.objects.get(venue=venue)

        entries = timetable.entries()

        self.assertEqual(entries.__len__(), 2)

        self.assertEqual(entries[0].day, 1)
        self.assertEqual(entries[0].time, 15)

        self.assertEqual(entries[1].day, 2)
        self.assertEqual(entries[1].time, 15)

    def test_timetable_entry_lecturers(self):
        entry = TimetableEntry.objects.filter(day=1).filter(time=9).first()
        lecturers = entry.lecturers()

        self.assertEqual(lecturers.__len__(), 1)
        self.assertEqual(lecturers[0].name, 'Bob Systems')

    def test_timetable_entry_venues(self):
        entry = TimetableEntry.objects.filter(day=1).filter(time=9).first()
        venues = entry.venues()

        self.assertEqual(venues.__len__(), 1)
        self.assertEqual(venues[0].code, 'AC201')


def create_test_data():
    # create the programmes
    programme1 = Programme(code='1BCT1', name='1BCT - Computer Science')
    programme1.save()

    programme2 = Programme(code='2BCT1', name='2BCT - Computer Science')
    programme2.save()

    # create the programme timetable for 1BCT1
    programme_timetable = ProgrammeTimetable(year=2016,
                                             semester=1,
                                             version=1,
                                             programme=programme1)
    programme_timetable.save()

    # create the venues
    venue1 = Venue(code='IT102', name='IT102')
    venue1.save()

    venue2 = Venue(code='IT106', name='IT106')
    venue2.save()

    venue3 = Venue(code='AC201', name='AC201')
    venue3.save()

    # create the venue timetable for IT102
    venue_timetable = VenueTimetable(year=2016,
                                     semester=1,
                                     version=1,
                                     venue=venue1)
    venue_timetable.save()

    # create the modules
    module1 = Module(code='CT101', name='Computer Systems', semester='1',
                     ects=5)
    module1.save()

    module2 = Module(code='CT216', name='Software Engineering', semester='1',
                     ects=5)
    module2.save()

    # add modules to programmes
    TakenBy(module=module1, programme=programme1, is_core=True).save()
    TakenBy(module=module2, programme=programme2, is_core=True).save()

    # create lecturers
    lecturer1 = Lecturer(name='Bob Systems', email='b.systems@nuigalway.ie')
    lecturer1.save()

    lecturer2 = Lecturer(name='John Comp', email='j.comp@nuigalway.ie')
    lecturer2.save()

    # add lecturers to modules
    TaughtBy(module=module1, lecturer=lecturer1).save()
    TaughtBy(module=module2, lecturer=lecturer2).save()

    # create entries
    # normal CT101 slot
    entry1 = TimetableEntry(module=module1,
                            day=1,
                            time=9,
                            semester=1,
                            year=2016,
                            is_lab_entry=False)
    entry1.save()

    # CT101 lab slot in IT102
    entry2 = TimetableEntry(module=module1,
                            day=1,
                            time=15,
                            semester=1,
                            year=2016,
                            is_lab_entry=True)
    entry2.save()

    # CT101 lab slot in IT106
    # not taken by 1BCT1!
    entry3 = TimetableEntry(module=module1,
                            day=1,
                            time=16,
                            semester=1,
                            year=2016,
                            is_lab_entry=True,
                            specific_to_programme=programme2)
    entry3.save()

    # normal CT216 slot
    entry4 = TimetableEntry(module=module2,
                            day=2,
                            time=9,
                            semester=1,
                            year=2016,
                            is_lab_entry=False)
    entry4.save()

    # CT216 lab slot in IT102
    entry5 = TimetableEntry(module=module2,
                            day=2,
                            time=15,
                            semester=1,
                            year=2016,
                            is_lab_entry=True)
    entry5.save()

    # add venues to entries
    EntryVenue(entry=entry1, venue=venue3).save()  # CT101 AC201
    EntryVenue(entry=entry2, venue=venue1).save()  # CT101 IT102
    EntryVenue(entry=entry3, venue=venue2).save()  # CT101 IT106
    EntryVenue(entry=entry4, venue=venue3).save()  # CT216 AC201
    EntryVenue(entry=entry5, venue=venue1).save()  # CT216 IT102

    # add lecturers to entries
    EntryLecturer(entry=entry1, lecturer=lecturer1).save()  # CT101 bob
    EntryLecturer(entry=entry2, lecturer=lecturer1).save()  # CT101 bob
    EntryLecturer(entry=entry3, lecturer=lecturer1).save()  # CT101 bob
    EntryLecturer(entry=entry4, lecturer=lecturer2).save()  # CT216 john
    EntryLecturer(entry=entry5, lecturer=lecturer2).save()  # CT216 john
