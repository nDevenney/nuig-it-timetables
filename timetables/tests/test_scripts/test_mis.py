from django.test import TestCase

from ssl import CERT_NONE
from unittest.mock import patch, Mock
from urllib.request import urlopen
from http.client import HTTPResponse
from urllib.error import URLError

from timetables.scripts import mis
from timetables.models import Programme, Module, TakenBy


class MisTestCase(TestCase):
    def setUp(self):
        Programme.objects.create(code='1BCT1',
                                 name='1BCT1 - BCT1 Bachelor of Science (Computer Science & Information Technology)')
        Module.objects.create(code='CT101', ects=10, name="Computing Systems",
                              semester='all year')
        TakenBy.objects.create(module=Module.objects.get(code='CT101'),
                               programme=Programme.objects.get(code='1BCT1'))

    def test_set_context(self):
        context = mis.set_context()

        self.assertEqual(context.verify_mode, CERT_NONE)

    @patch('timetables.scripts.mis.MisProgrammeParser')
    @patch('timetables.scripts.mis.urlopen')
    def test_mis_programme_url(self, urlopen, MisProgrammeParser):
        expected_url = 'https://www.mis.nuigalway.ie/regexam/cmrs_module_detail.asp?ay=2016&instance_code=1BCT1'

        fake_response = MockResponse('<html>mock</html>')
        urlopen.return_value = fake_response
        mis.get_programme_information(2016, '1BCT1')

        self.assertEqual(urlopen.call_args_list[0][0][0], expected_url)

    @patch('timetables.scripts.mis.MisProgrammeParser')
    @patch('timetables.scripts.mis.urlopen')
    def test_failure_to_connect(self, urlopen, MisProgrammeParser):
        urlopen.side_effect = URLError('Failure to connect')
        status = mis.get_programme_information(2016, '1BCT1')
        self.assertEqual(status.status, 'Failure to connect')

        urlopen.side_effect = RuntimeError('Failure to connect')
        status = mis.get_programme_information(2016, '1BCT1')
        self.assertEqual(status.status, 'Failure to connect')

    @patch('timetables.scripts.mis.MisProgrammeParser', autospec=True)
    @patch('timetables.scripts.mis.urlopen')
    def test_latin_1_decoded_for_programme(self, urlopen, parser):    
        fake_response = MockResponse('<html>mock</html>')
        urlopen.return_value = fake_response

        mis.get_programme_information(2016, '1BCT1')

        # tuple unpacking
        call = parser.mock_calls[1]
        name, args, kwargs = call

        # html from MIS is encoding in latin_1, this would return false for
        # isinstance(html, str). This tests to see that the HTML is decoded to
        # unicode before being sent to the parser
        self.assertTrue(isinstance(args[0], str))

    @patch('timetables.scripts.mis.MisProgrammeParser.get_information')
    @patch('timetables.scripts.mis.urlopen')
    def test_programme_does_not_exist(self, urlopen, get_information):    
        fake_response = MockResponse('<html>mock</html>')
        urlopen.return_value = fake_response

        get_information.side_effect = RuntimeError('Programme does not exist')

        status = mis.get_programme_information(2016, '1BCT1')
        self.assertEqual(status.status, "Programme does not exist")

    @patch('timetables.scripts.mis.MisProgrammeParser.get_information')
    @patch('timetables.scripts.mis.urlopen')
    def test_programme_saved(self, urlopen, get_information):    
        fake_response = MockResponse('<html>mock</html>')
        urlopen.return_value = fake_response
        get_information.return_value = {
            'programme': Programme(code='2BCT1',
                                   name='2BCT1 - BCT1 Bachelor of Science (Computer Science & Information Technology)'),
            'modules': [],
            'taken_by': []
        }

        mis.get_programme_information(2016, '2BCT1')

        self.assertEqual(Programme.objects.all().__len__(), 2)

    @patch('timetables.scripts.mis.MisProgrammeParser.get_information')
    @patch('timetables.scripts.mis.urlopen')
    def test_programme_updated(self, urlopen, get_information):    
        fake_response = MockResponse('<html>mock</html>')
        new_name = '1BCT1 - BCT1 Bachelor of Science (IT)'
        urlopen.return_value = fake_response
        get_information.return_value = {
            'programme': Programme(code='1BCT1',
                                   name=new_name),
            'modules': [],
            'taken_by': []
        }

        mis.get_programme_information(2016, '1BCT1')

        self.assertEqual(Programme.objects.all().__len__(), 1)
        self.assertEqual(Programme.objects.get(code='1BCT1').name, new_name)

    @patch('timetables.scripts.mis.get_module_information')
    @patch('timetables.scripts.mis.MisProgrammeParser.get_information')
    @patch('timetables.scripts.mis.urlopen')
    def test_module_saved(self, urlopen, get_information,
                          get_module_information):
        fake_response = MockResponse('<html>mock</html>')
        urlopen.return_value = fake_response
        get_information.return_value = {
            'programme': Programme(code='1BCT1',
                                   name='1BCT1 - BCT1 Bachelor of Science (Computer Science & Information Technology)'),
            'modules': [Module(code='CT103', ects=10, name="Programming",
                               semester='all year')],
            'taken_by': []
        }

        mis.get_programme_information(2016, '1BCT1')

        self.assertEqual(Module.objects.all().__len__(), 2)

    @patch('timetables.scripts.mis.get_module_information')
    @patch('timetables.scripts.mis.MisProgrammeParser.get_information')
    @patch('timetables.scripts.mis.urlopen')
    def test_module_updated(self, urlopen, get_information,
                            get_module_information):
        fake_response = MockResponse('<html>mock</html>')
        urlopen.return_value = fake_response
        get_information.return_value = {
            'programme': Programme(code='1BCT1',
                                   name='1BCT1 - BCT1 Bachelor of Science (Computer Science & Information Technology)'),
            'modules': [Module(code='CT101', ects=10,
                               name="Computer Systems I",
                               semester='all year')],
            'taken_by': []
        }

        mis.get_programme_information(2016, '1BCT1')

        self.assertEqual(Module.objects.all().__len__(), 1)

        # module name changed from "Computing Systems" to "Computer Systems I"
        self.assertEqual(Module.objects.get(code='CT101').name,
                         "Computer Systems I")

    @patch('timetables.scripts.mis.get_module_information')
    @patch('timetables.scripts.mis.MisProgrammeParser.get_information')
    @patch('timetables.scripts.mis.urlopen')
    def test_taken_by_saved(self, urlopen, get_information,
                            get_module_information):
        fake_response = MockResponse('<html>mock</html>')
        urlopen.return_value = fake_response
        get_information.return_value = {
            'programme': Programme(code='1BCT1',
                                   name='1BCT1 - BCT1 Bachelor of Science (Computer Science & Information Technology)'),
            'modules': [Module(code='CT103', ects=10,
                               name="Programming",
                               semester='all year'),
                        Module(code='CT101', ects=10,
                               name='Computing Systems',
                               semester='all year')],
            'taken_by': [{
                'module': 'CT103',
                'is_core': True
            }]
        }

        mis.get_programme_information(2016, '1BCT1')
        self.assertEqual(TakenBy.objects.all().__len__(), 2)

    @patch('timetables.scripts.mis.get_module_information')
    @patch('timetables.scripts.mis.MisProgrammeParser.get_information')
    @patch('timetables.scripts.mis.urlopen')
    def test_taken_by_updated(self, urlopen, get_information,
                            get_module_information):
        fake_response = MockResponse('<html>mock</html>')
        urlopen.return_value = fake_response
        get_information.return_value = {
            'programme': Programme(code='1BCT1',
                                   name='1BCT1 - BCT1 Bachelor of Science (Computer Science & Information Technology)'),
            'modules': [Module(code='CT101', ects=10,
                               name="Programming",
                               semester='all year')],
            'taken_by': [{
                'module': 'CT101',
                'is_core': False
            }]
        }

        mis.get_programme_information(2016, '1BCT1')
        self.assertEqual(TakenBy.objects.all().__len__(), 1)

        # CT101 is now optional instead of core
        self.assertFalse(TakenBy.objects.all()[0].is_core)

    @patch('timetables.scripts.mis.get_module_information')
    @patch('timetables.scripts.mis.MisProgrammeParser.get_information')
    @patch('timetables.scripts.mis.urlopen')
    def test_outdated_taken_bys_deleted(self, urlopen, get_information,
                            get_module_information):
        fake_response = MockResponse('<html>mock</html>')
        urlopen.return_value = fake_response
        get_information.return_value = {
            'programme': Programme(code='1BCT1',
                                   name='1BCT1 - BCT1 Bachelor of Science (Computer Science & Information Technology)'),
            'modules': [Module(code='CT103', ects=10,
                               name="Programming",
                               semester='all year')],
            'taken_by': [{
                'module': 'CT103',
                'is_core': True
            }]
        }

        mis.get_programme_information(2016, '1BCT1')

        # CT101 is not taken by 1BCT1 anymore
        self.assertEqual(TakenBy.objects.all().__len__(), 1)

    @patch('timetables.scripts.mis.MisProgrammeParser')
    @patch('timetables.scripts.mis.urlopen')
    def test_success_status_returned(self, urlopen, parser):
        expected_url = 'https://www.mis.nuigalway.ie/regexam/cmrs_module_detail.asp?ay=2016&instance_code=1BCT1'

        fake_response = MockResponse('<html>mock</html>')
        urlopen.return_value = fake_response
        status = mis.get_programme_information(2016, '1BCT1')

        self.assertEqual(status.status, "Success")

    @patch('timetables.scripts.mis.MisModuleParser')
    @patch('timetables.scripts.mis.urlopen')
    def test_mis_module_url(self, urlopen, MisProgrammeParser):
        expected_url = 'https://www.mis.nuigalway.ie/regexam/cmrs_module_detail.asp?ay=2016&module=CT101'

        fake_response = MockResponse('<html>mock</html>')
        urlopen.return_value = fake_response
        mis.get_module_information(2016, Module(code='CT101'))

        self.assertEqual(urlopen.call_args_list[0][0][0], expected_url)

    @patch('timetables.scripts.mis.MisModuleParser')
    @patch('timetables.scripts.mis.urlopen')
    def test_failure_to_connect_for_modules(self, urlopen, MisModuleParser):
        urlopen.side_effect = URLError('Failure to connect')
        status = mis.get_module_information(2016, Module(code='CT101'))
        self.assertEqual(status.status, 'Failure to connect')

        urlopen.side_effect = RuntimeError('Failure to connect')
        status = mis.get_module_information(2016, Module(code='CT101'))
        self.assertEqual(status.status, 'Failure to connect')

    @patch('timetables.scripts.mis.MisModuleParser', autospec=True)
    @patch('timetables.scripts.mis.urlopen')
    def test_latin_1_decoded_for_module(self, urlopen, parser):
        fake_response = MockResponse('<html>mock</html>')
        urlopen.return_value = fake_response

        mis.get_module_information(2016, Module(code='CT101'))

        # tuple unpacking
        call = parser.mock_calls[1]
        name, args, kwargs = call

        # html from MIS is encoding in latin_1, this would return false for
        # isinstance(html, str). This tests to see that the HTML is decoded to
        # unicode before being sent to the parser
        self.assertTrue(isinstance(args[0], str))

    @patch('timetables.scripts.mis.MisModuleParser.get_information',
           autospec=True)
    @patch('timetables.scripts.mis.urlopen')
    def test_module_parser_called(self, urlopen, get_information):
        fake_response = MockResponse('<html>mock</html>')
        urlopen.return_value = fake_response

        mis.get_module_information(2016, Module(code='CT101'))

        self.assertTrue(get_information.mock_calls.__len__() == 1)


# mocks for testing
class MockResponse():
    def __init__(self, data):
        self.data = data

    def read(self):
        return self.data.encode(encoding='latin_1')
