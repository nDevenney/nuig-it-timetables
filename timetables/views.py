# views for the timetables app (acts as a controller in the MVC architecture)

from django.shortcuts import render
from django.utils import timezone
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from timetables.models import *
from timetables.serializers import *
from rest_framework.permissions import IsAdminUser
from django.contrib.auth import authenticate, login, logout

from timetables.scripts.mis import get_programme_information


class ProgrammeViewSet(viewsets.ModelViewSet):
    http_method_names = ['get', 'delete']

    queryset = Programme.objects.all()
    serializer_class = ProgrammeSerializer

    def destroy(self, request, pk=None):
        if not request.user.is_superuser:
            return Response(status=status.HTTP_403_FORBIDDEN)

        programme = Programme.objects.get(code=pk)

        programme.delete()

        # delete 'orphaned' modules (not taken by anyone)
        modules = Module.objects.all()
        taken_by = TakenBy.objects.all()

        for m in modules:
            is_orphan = True

            for t in taken_by:
                if t.module == m:
                    is_orphan = False
                    break

            if is_orphan:
                m.delete()

        return Response(status.HTTP_204_NO_CONTENT)


class ModuleViewSet(viewsets.ModelViewSet):
    http_method_names = ['get', 'put']

    queryset = Module.objects.all()
    serializer_class = ModuleSerializer

    def update(self, request, pk=None):
        if not request.user.is_superuser:
            return Response(status=status.HTTP_403_FORBIDDEN)

        module = Module.objects.get(code=request.data['code'])

        module.short_name = request.data['shortName']
        module.save()

        return Response(status=status.HTTP_200_OK)


# these view sets are used to return ALL timetables regardless of semester/year
class ProgrammeTimetableViewSet(viewsets.ModelViewSet):
    http_method_names = ['get']

    queryset = ProgrammeTimetable.objects.all()
    serializer_class = ProgrammeTimetableSerializer


class VenueTimetableViewSet(viewsets.ModelViewSet):
    http_method_names = ['get', 'post']

    queryset = VenueTimetable.objects.all()
    serializer_class = VenueTimetableSerializer

    def create(self, request):
        if not request.user.is_superuser:
            return Response(status=status.HTTP_403_FORBIDDEN)

        venue = Venue.objects.get(code=request.data['code'])

        # check for duplicates
        if (VenueTimetable.objects.filter(venue=venue)
                .filter(semester=request.data['semester'])
                .filter(year=request.data['year'])
                .__len__() > 0):
            return Response(status=status.HTTP_400_BAD_REQUEST)

        timetable = VenueTimetable(year=request.data['year'],
                                   semester=request.data['semester'],
                                   venue=venue,
                                   is_public=request.data['is_public'])

        timetable.save()
        serializer = VenueTimetableSerializer(timetable)

        return Response(serializer.data)


class LecturerViewSet(viewsets.ModelViewSet):
    permission_classes = ((IsAdminUser, ))
    http_method_names = ['get', 'post', 'put', 'delete']

    # required as DRF normally interprets a '.' in an email as a formatting
    # option
    lookup_value_regex = '[A-Za-z0-9.@]+'

    queryset = Lecturer.objects.all()
    serializer_class = LecturerSerializer

    def update(self, request, pk=None):
        if not request.user.is_superuser:
            return Response(status=status.HTTP_403_FORBIDDEN)

        lecturer = Lecturer.objects.get(email=request.data['oldEmail'])

        if request.data['oldEmail'] == request.data['newEmail']:
            data = {
                'email': request.data['oldEmail'],
                'name': request.data['name']
            }

            serializer = LecturerSerializer(lecturer, data=data)

            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
        else:
            # need to update lecturer
            new_lecturer = Lecturer(email=request.data['newEmail'],
                                    name=request.data['name'])
            new_lecturer.save()

            # need to update all modules taught by lecturer
            taught_by = TaughtBy.objects.filter(lecturer=lecturer)

            for t in taught_by:
                t.lecturer = new_lecturer
                t.save()

            # need to update all entries taught by lecturer
            entries = EntryLecturer.objects.filter(lecturer=lecturer)

            for e in entries:
                e.lecturer = new_lecturer
                e.save()

            # delete old lecturer row
            lecturer.delete()

            data = {
                'email': request.data['newEmail'],
                'name': request.data['name']
            }

            serializer = LecturerSerializer(new_lecturer, data=data)

            if serializer.is_valid():
                return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class VenueViewSet(viewsets.ModelViewSet):
    permission_classes = ((IsAdminUser, ))
    http_method_names = ['get', 'post', 'put', 'delete']

    queryset = Venue.objects.all()
    serializer_class = VenueSerializer

    def update(self, request, pk=None):
        if not request.user.is_superuser:
            return Response(status=status.HTTP_403_FORBIDDEN)

        venue = Venue.objects.get(code=request.data['oldCode'])

        if request.data['oldCode'] == request.data['newCode']:
            data = {
                'code': request.data['oldCode'],
                'name': request.data['name']
            }

            serializer = VenueSerializer(venue, data=data)

            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
        else:
            # need to update venue
            new_venue = Venue(code=request.data['newCode'],
                              name=request.data['name'])
            new_venue.save()

            # need to update all entries for this venue
            entries = EntryVenue.objects.filter(venue=venue)

            for e in entries:
                e.venue = new_venue
                e.save()

            # need to update timetables for this venue
            timetables = VenueTimetable.objects.filter(venue=venue)

            for t in timetables:
                t.venue = new_venue
                t.save()

            # delete old venue row
            venue.delete()

            data = {
                'code': request.data['newCode'],
                'name': request.data['name']
            }

            serializer = VenueSerializer(new_venue, data=data)

            if serializer.is_valid():
                return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TimetableEntryViewSet(viewsets.ModelViewSet):
    http_method_names = ['get', 'post', 'delete']

    queryset = TimetableEntry.objects.all()
    serializer_class = TimetableEntrySerializer

    def create(self, request):
        if not request.user.is_superuser:
            return Response(status=status.HTTP_403_FORBIDDEN)

        # get module
        module = Module.objects.get(code=request.data['module'])

        if request.data['specificToProgramme'] == 'All':
            specific_to_programme = None
        else:
            specific_to_programme = Programme.objects.get(
                code=request.data['specificToProgramme'])

        # create timetable entry row
        entry = TimetableEntry(module=module, day=request.data['day'],
                               time=request.data['time'],
                               semester=request.data['semester'],
                               year=request.data['year'],
                               is_lab_entry=request.data['isLabEntry'],
                               specific_to_programme=specific_to_programme)

        # check for split
        other_entries = (TimetableEntry.objects.filter(day=request.data['day'])
                         .filter(time=request.data['time'])
                         .filter(semester=request.data['semester'])
                         .filter(year=request.data['year']))

        for e in other_entries:
            for x in e.module.taken_by():
                for y in module.taken_by():
                    if x.programme.code == y.programme.code:
                        e.split = 'A'
                        e.save()
                        entry.split = 'B'

        entry.save()

        # create entry venue rows
        for v in request.data['venues']:
            venue = Venue.objects.get(code=v)
            EntryVenue(entry=entry, venue=venue).save()

        # create entry lecturer rows
        if 'lecturers' in request.data:
            for l in request.data['lecturers']:
                lecturer = Lecturer.objects.get(email=l)
                EntryLecturer(lecturer=lecturer, entry=entry).save()

            # add lecturers to taughtby if need be (dead code?)
            for l in request.data['lecturers']:
                lecturer = Lecturer.objects.get(email=l)

                if (TaughtBy.objects.filter(lecturer=lecturer)
                        .filter(module=module).__len__() < 1):
                    TaughtBy(module=module, lecturer=lecturer).save()

        # find timetables effected and update their version info
        # first degree programme timetables
        for p in module.taken_by():
            timetable = ProgrammeTimetable.objects.get(programme=p.programme,
                                                       year=request.data['year'],
                                                       semester=request.data['semester'])

            timetable.version += 1
            timetable.date_updated = timezone.now()
            timetable.save()

        # then venue timetables
        for v in request.data['venues']:
            venue = Venue.objects.get(code=v)

            if (VenueTimetable.objects.filter(venue=v)
                                      .filter(year=request.data['year'])
                                      .filter(semester=request.data['semester'])
                                      .__len__() == 1):
                timetable = VenueTimetable.objects.get(venue=v, year=request.data['year'], semester=request.data['semester']) 
                timetable.version += 1
                timetable.date_updated = timezone.now()
                timetable.save()

        serializer = TimetableEntrySerializer(entry)

        return Response(serializer.data)

    def destroy(self, request, pk=None):
        if not request.user.is_superuser:
            return Response(status=status.HTTP_403_FORBIDDEN)

        entry = TimetableEntry.objects.get(id=pk)

        if entry.split == 'A' or entry.split == 'B':
            other_entries = (TimetableEntry.objects.filter(day=entry.day)
                             .filter(time=entry.time)
                             .filter(semester=entry.semester)
                             .filter(year=entry.year))

            for e in other_entries:
                for x in e.module.taken_by():
                    for y in entry.module.taken_by():
                        if x.programme.code == y.programme.code:
                            e.split = ''
                            e.save()

        entry.delete()

        return Response(status.HTTP_204_NO_CONTENT)


class TaughtByViewSet(viewsets.ModelViewSet):
    permission_classes = ((IsAdminUser, ))

    queryset = TaughtBy.objects.all()
    serializer_class = TaughtBySerializer

    def create(self, request):
        lecturer = Lecturer.objects.get(email=request.data['lecturer'])
        module = Module.objects.get(code=request.data['module'])

        if (TaughtBy.objects.filter(module=module).filter(lecturer=lecturer)
                .__len__() > 0):
            return Response(status.HTTP_200_OK)

        taughtby = TaughtBy(module=module, lecturer=lecturer)
        taughtby.save()

        # update existing entries if theres only one module lecturer
        if TaughtBy.objects.filter(module=module).__len__() == 1:
            active_semester = ActiveSemester.objects.all().first()

            entries = TimetableEntry.objects.filter(year=active_semester.year,
                                                    semester=active_semester.semester,
                                                    module=module)

            for e in entries:
                # dont make duplicates
                if EntryLecturer.objects.filter(entry=e, lecturer=lecturer).__len__ > 0:
                    continue

                EntryLecturer(entry=e, lecturer=lecturer).save()

        return Response(TaughtBySerializer(taughtby).data)


@api_view(['POST'])
@permission_classes((IsAdminUser, ))
def programme_timetable_copy(request, code, year, semester):
    # get programme
    programme = Programme.objects.get(code=code)

    # get programme modules
    taken_by = TakenBy.objects.filter(programme=programme)
    modules = []

    for t in taken_by:
        modules.append(t.module)

    # get last year's timetable's entries
    last_year_entries = []

    for m in modules:
        entries = list(TimetableEntry.objects.filter(module=m,
                                                     semester=semester,
                                                     year=(int(year)-1)))
        last_year_entries.extend(entries)

    # create copies and update the year
    for e in last_year_entries:
        entry_lecturers = EntryLecturer.objects.filter(entry=e)
        entry_venues = EntryVenue.objects.filter(entry=e)

        e.pk = None
        e.year = year
        e.save()

        for el in entry_lecturers:
            EntryLecturer(entry=e, lecturer=el.lecturer).save()

        for ev in entry_venues:
            EntryVenue(entry=e, venue=ev.venue).save()

    return Response("success")


@api_view(['GET', 'PUT'])
def active_semester(request):
    active_semester = ActiveSemester.objects.all().first()

    if request.method == 'GET':
        serializer = ActiveSemesterSerializer(active_semester)
        return Response(serializer.data)

    elif request.method == 'PUT':
        if not request.user.is_superuser:
            return Response(status=status.HTTP_403_FORBIDDEN)

        serializer = ActiveSemesterSerializer(active_semester,
                                              data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# dead code?
@api_view(['GET'])
def programme_modules(request, code):
    programme = Programme.objects.get(code=code)
    taken_by = TakenBy.objects.filter(programme=programme)
    modules = []

    for t in taken_by:
        modules.append(t.module)

    serializer = ModuleSerializer(modules, many=True)

    return Response(serializer.data)


@api_view(['GET', 'PUT', 'DELETE'])
def programme_timetable(request, code, year, semester):
    programme = Programme.objects.get(code=code)
    timetable = ProgrammeTimetable.objects.get(programme=programme,
                                               year=year,
                                               semester=semester)

    if request.method == 'GET':
        if not timetable.is_public and not request.user.is_superuser:
            return Response(status=status.HTTP_403_FORBIDDEN)

        serializer = ProgrammeTimetableSerializer(timetable)

        return Response(serializer.data)
    elif request.method == 'PUT':
        if not request.user.is_superuser:
            return Response(status=status.HTTP_403_FORBIDDEN)

        timetable.is_public = request.data['is_public']
        timetable.save()

        serializer = ProgrammeTimetableSerializer(timetable)

        return Response(serializer.data)
    elif request.method == 'DELETE':
        if not request.user.is_superuser:
            return Response(status=status.HTTP_403_FORBIDDEN)

        timetable.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'PUT', 'DELETE'])
def venue_timetable(request, code, year, semester):
    venue = Venue.objects.get(code=code)
    timetable = VenueTimetable.objects.get(venue=venue,
                                           year=year,
                                           semester=semester)

    if request.method == 'GET':
        serializer = VenueTimetableSerializer(timetable)

        return Response(serializer.data)
    elif request.method == 'PUT':
        if not request.user.is_superuser:
            return Response(status=status.HTTP_403_FORBIDDEN)

        timetable.is_public = request.data['is_public']
        timetable.save()

        serializer = VenueTimetableSerializer(timetable)

        return Response(serializer.data)
    elif request.method == 'DELETE':
        timetable.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST'])
def programme_timetable_all(request, year, semester):
    if request.method == 'GET':
        if request.user.is_superuser:
            timetables = ProgrammeTimetable.objects.filter(year=year,
                                                           semester=semester)
        else:
            timetables = ProgrammeTimetable.objects.filter(year=year,
                                                           semester=semester,
                                                           is_public=True)

        serializer = ProgrammeTimetableSerializer(timetables, many=True)

        return Response(serializer.data)
    elif request.method == 'POST':
        if not request.user.is_superuser:
            return Response(status=status.HTTP_403_FORBIDDEN)

        serializer = ProgrammeTimetableSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
def venue_timetable_all(request, year, semester):
    if request.method == 'GET':
        if request.user.is_superuser:
            timetables = VenueTimetable.objects.filter(year=year,
                                                       semester=semester)
        else:
            timetables = VenueTimetable.objects.filter(year=year,
                                                       semester=semester,
                                                       is_public=True)

        serializer = VenueTimetableSerializer(timetables, many=True)

        return Response(serializer.data)
    elif request.method == 'POST':
        if not request.user.is_superuser:
            return Response(status=status.HTTP_403_FORBIDDEN)

        serializer = VenueTimetableSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes((IsAdminUser, ))
def module_timetable_entries(request, module):
    module = Module.objects.get(code=module)

    entries = TimetableEntry.objects.filter(module=module)

    serializer = TimetableEntrySerializer(entries, many=True)

    return Response(serializer.data)


@api_view(['GET'])
@permission_classes((IsAdminUser, ))
def module_taken_by(request, module):
    module = Module.objects.get(code=module)
    taken_by = TakenBy.objects.filter(module=module)

    serializer = TakenBySerializer(taken_by, many=True)

    return Response(serializer.data)


@api_view(['GET'])
@permission_classes((IsAdminUser, ))
def module_taught_by(request, module):
    module = Module.objects.get(code=module)
    taught_by = TaughtBy.objects.filter(module=module)

    serializer = TaughtBySerializer(taught_by, many=True)

    return Response(serializer.data)


@api_view(['GET'])
@permission_classes((IsAdminUser, ))
def batch_update(request, year, code):
    status = get_programme_information(year, code)

    if status.status != 'Success':
        serializer = BatchUpdateSerializer(status)

        return Response(serializer.data)

    # create timetables if they don't exist
    programme = Programme.objects.get(code=code)

    sem1_timetable = (ProgrammeTimetable.objects.filter(year=year)
                      .filter(semester=1)
                      .filter(programme=programme))

    sem2_timetable = (ProgrammeTimetable.objects.filter(year=year)
                      .filter(semester=2)
                      .filter(programme=programme))

    if sem1_timetable.__len__() == 0:
        timetable = ProgrammeTimetable(year=year,
                                       semester=1,
                                       programme=programme,
                                       version=1,
                                       is_public=False)
        timetable.save()

    if sem2_timetable.__len__() == 0:
        timetable = ProgrammeTimetable(year=year,
                                       semester=2,
                                       programme=programme,
                                       version=1,
                                       is_public=False)
        timetable.save()

    serializer = BatchUpdateSerializer(status)

    return Response(serializer.data)


# base Django template which delegates routes to angular ui-router
def base(request):
    return render(request, 'timetables/base.html')
