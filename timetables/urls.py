# url routing for the timetables app

from django.conf.urls import include, url
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'programme', views.ProgrammeViewSet, base_name="programme")
router.register(r'module', views.ModuleViewSet, base_name="module")
router.register(r'lecturer', views.LecturerViewSet, base_name="lecturer")
router.register(r'venue', views.VenueViewSet, base_name="venue")
router.register(r'entry', views.TimetableEntryViewSet, base_name="entry")
router.register(r'taughtby', views.TaughtByViewSet, base_name="taughtby")
router.register(r'timetable/programme', views.ProgrammeTimetableViewSet,
                base_name="programmetimetable")
router.register(r'timetable/venue', views.VenueTimetableViewSet,
                base_name="venuetimetable")

urlpatterns = [
    url(r'^api/batchupdate/(?P<year>[0-9]+)/(?P<code>[0-9A-Za-z]+)/$',
        views.batch_update),

    url(r'^api/module/(?P<module>[0-9A-Z]+)/takenby/$', views.module_taken_by),
    url(r'^api/module/(?P<module>[0-9A-Z]+)/taughtby/$',
        views.module_taught_by),
    url(r'^api/module/(?P<module>[0-9A-Z]+)/entries/$',
        views.module_timetable_entries),

    url(r'^api/programme/(?P<code>[0-9A-Z]+)/modules/$',
        views.programme_modules),

    url(r'^api/timetable/(?P<year>[0-9]+)/(?P<semester>[0-9]+)/programme/$',
        views.programme_timetable_all),
    url(r'^api/timetable/(?P<year>[0-9]+)/(?P<semester>[0-9]+)/venue/$',
        views.venue_timetable_all),
    url(r'^api/timetable/(?P<year>[0-9]+)/(?P<semester>[0-9]+)/programme/(?P<code>[0-9A-Z]+)/$',
        views.programme_timetable),
    url(r'^api/timetable/(?P<year>[0-9]+)/(?P<semester>[0-9]+)/programme/(?P<code>[0-9A-Z]+)/copy/$',
        views.programme_timetable_copy),
    url(r'^api/timetable/(?P<year>[0-9]+)/(?P<semester>[0-9]+)/venue/(?P<code>[a-zA-Z0-9 -]+)/$',
        views.venue_timetable),

    url(r'^api/activesemester/$', views.active_semester),

    url(r'^api/', include(router.urls)),

    url('^.*$', views.base, name='base'),
]
