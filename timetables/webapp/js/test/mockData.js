"use strict";

// batch update mock data
var mockBatchUpdateStatus = function() {
	return {
		'status': 'Success',
		'timestamp': '2017-01-01T00:00:00'
	};
}

// programme mock data
var mockProgramme = function() {
	return {
		'code': '1BCT1',
		'name': '1BCT - Computer Science and I.T.'
	};
};

var mockNewProgramme = function() {
	return {
		'code': '3BCT1',
		'name': '3BCT - Computer Science and I.T.'
	};
};

var mockProgrammes = function() {
	return [
		mockProgramme(), {
			'code': '2BCT1',
			'name': '2BCT - Computer Science and I.T.'
		}
	];
};

var mockProgrammesWithStatus = function() {
	return [
		mockProgramme(), {
			'code': '2BCT1',
			'name': '2BCT - Computer Science and I.T.'
		}
	];
};

// timetable mock data
var mockProgrammeTimetable = function() {
	return {  
	    "year":2016,
	    "semester":1,
	    "version":1,
	    "programme":{  
	        "name":"1BCT1 - BCT1 Bachelor of Science (Computer Science & Information Technology)",
	        "code":"1BCT1"
	    },
	    "date_updated":"2017-01-15T16:11:41.713783Z",
	    "entries":[  
	        {  
	            "day":1,
	            "time":10,
	            "semester":1,
	            "year":2016,
	            "is_lab_entry":false,
	            "module":{  
	                "code":"EE130",
	                "ects":5,
	                "name":"Fundamentals of Electrical & Electronic Engineering I",
	                "short_name":"Fundamentals of EEE",
	                "semester":"1"
	            },
	            "lecturers":[  
	                {  
	                    "email":"j.breslin@nuigalway.ie",
	                    "name":"John Breslin"
	                }
	            ],
	            "venues":[  
	                {  
	                    "code":"ENG-2001",
	                    "name":"ENG-2001"
	                }
	            ],
	            "split":"",
	            "specific_to_programme":null
	        },
	        {  
	            "day":1,
	            "time":11,
	            "semester":1,
	            "year":2016,
	            "is_lab_entry":false,
	            "module":{  
	                "code":"CT103",
	                "ects":10,
	                "name":"Programming",
	                "short_name":"",
	                "semester":"all year"
	            },
	            "lecturers":[  
	                {  
	                    "email":"o.molloy@nuigalway.ie",
	                    "name":"Owen Molloy"
	                }
	            ],
	            "venues":[  
	                {  
	                    "code":"Dillon Theatre",
	                    "name":"Dillon Theatre"
	                }
	            ],
	            "split":"",
	            "specific_to_programme":null
	        },
	        {  
	            "day":1,
	            "time":13,
	            "semester":1,
	            "year":2016,
	            "is_lab_entry":false,
	            "module":{  
	                "code":"MA160",
	                "ects":10,
	                "name":"Mathematics",
	                "short_name":"",
	                "semester":"all year"
	            },
	            "lecturers":[  
	                {  
	                    "email":"j.cruickshank@nuigalway.ie",
	                    "name":"James Cruickshank"
	                }
	            ],
	            "venues":[  
	                {  
	                    "code":"AC213",
	                    "name":"AC213"
	                }
	            ],
	            "split":"A",
	            "specific_to_programme":null
	        },
	        {  
	            "day":1,
	            "time":13,
	            "semester":1,
	            "year":2016,
	            "is_lab_entry":false,
	            "module":{  
	                "code":"MA190",
	                "ects":10,
	                "name":"Mathematics (Honours)",
	                "short_name":"Mathematics (H)",
	                "semester":"all year"
	            },
	            "lecturers":[  
	                {  
	                    "email":"j.burns@nuigalway.ie",
	                    "name":"John Burns"
	                }
	            ],
	            "venues":[  
	                {  
	                    "code":"IT250",
	                    "name":"IT250"
	                }
	            ],
	            "split":"B",
	            "specific_to_programme":null
	        },
	        {  
	            "day":1,
	            "time":14,
	            "semester":1,
	            "year":2016,
	            "is_lab_entry":false,
	            "module":{  
	                "code":"CT101",
	                "ects":10,
	                "name":"Computing Systems",
	                "short_name":"",
	                "semester":"all year"
	            },
	            "lecturers":[  
	                {  
	                    "email":"p.bigioi@nuigalway.ie",
	                    "name":"Petronel Bigioi"
	                }
	            ],
	            "venues":[  
	                {  
	                    "code":"AC202",
	                    "name":"AC202"
	                }
	            ],
	            "split":"",
	            "specific_to_programme":null
	        },
	        {  
	            "day":1,
	            "time":15,
	            "semester":1,
	            "year":2016,
	            "is_lab_entry":false,
	            "module":{  
	                "code":"CT101",
	                "ects":10,
	                "name":"Computing Systems",
	                "short_name":"",
	                "semester":"all year"
	            },
	            "lecturers":[  
	                {  
	                    "email":"p.bigioi@nuigalway.ie",
	                    "name":"Petronel Bigioi"
	                }
	            ],
	            "venues":[  
	                {  
	                    "code":"IT125",
	                    "name":"IT125"
	                }
	            ],
	            "split":"",
	            "specific_to_programme":null
	        },
	        {  
	            "day":1,
	            "time":16,
	            "semester":1,
	            "year":2016,
	            "is_lab_entry":false,
	            "module":{  
	                "code":"EE130",
	                "ects":5,
	                "name":"Fundamentals of Electrical & Electronic Engineering I",
	                "short_name":"Fundamentals of EEE",
	                "semester":"1"
	            },
	            "lecturers":[  
	                {  
	                    "email":"j.breslin@nuigalway.ie",
	                    "name":"John Breslin"
	                }
	            ],
	            "venues":[  
	                {  
	                    "code":"ENG-2002",
	                    "name":"ENG-2002"
	                }
	            ],
	            "split":"",
	            "specific_to_programme":null
	        },
	        {  
	            "day":1,
	            "time":17,
	            "semester":1,
	            "year":2016,
	            "is_lab_entry":true,
	            "module":{  
	                "code":"CT101",
	                "ects":10,
	                "name":"Computing Systems",
	                "short_name":"",
	                "semester":"all year"
	            },
	            "lecturers":[  
	                {  
	                    "email":"p.bigioi@nuigalway.ie",
	                    "name":"Petronel Bigioi"
	                }
	            ],
	            "venues":[  
	                {  
	                    "code":"IT102",
	                    "name":"IT102"
	                },
	                {  
	                    "code":"IT106",
	                    "name":"IT106"
	                }
	            ],
	            "split":"",
	            "specific_to_programme":"1BCT1"
	        },
	        {  
	            "day":1,
	            "time":18,
	            "semester":1,
	            "year":2016,
	            "is_lab_entry":true,
	            "module":{  
	                "code":"CT101",
	                "ects":10,
	                "name":"Computing Systems",
	                "short_name":"",
	                "semester":"all year"
	            },
	            "lecturers":[  
	                {  
	                    "email":"p.bigioi@nuigalway.ie",
	                    "name":"Petronel Bigioi"
	                }
	            ],
	            "venues":[  
	                {  
	                    "code":"IT102",
	                    "name":"IT102"
	                },
	                {  
	                    "code":"IT106",
	                    "name":"IT106"
	                }
	            ],
	            "split":"",
	            "specific_to_programme":"1BCT1"
	        },
	        {  
	            "day":2,
	            "time":10,
	            "semester":1,
	            "year":2016,
	            "is_lab_entry":false,
	            "module":{  
	                "code":"MA160",
	                "ects":10,
	                "name":"Mathematics",
	                "short_name":"",
	                "semester":"all year"
	            },
	            "lecturers":[  
	                {  
	                    "email":"j.cruickshank@nuigalway.ie",
	                    "name":"James Cruickshank"
	                }
	            ],
	            "venues":[  
	                {  
	                    "code":"Tyndall Theatre",
	                    "name":"Tyndall Theatre"
	                }
	            ],
	            "split":"A",
	            "specific_to_programme":null
	        },
	        {  
	            "day":2,
	            "time":10,
	            "semester":1,
	            "year":2016,
	            "is_lab_entry":false,
	            "module":{  
	                "code":"MA190",
	                "ects":10,
	                "name":"Mathematics (Honours)",
	                "short_name":"Mathematics (H)",
	                "semester":"all year"
	            },
	            "lecturers":[  
	                {  
	                    "email":"j.burns@nuigalway.ie",
	                    "name":"John Burns"
	                }
	            ],
	            "venues":[  
	                {  
	                    "code":"Anderson Theatre",
	                    "name":"Anderson Theatre"
	                }
	            ],
	            "split":"B",
	            "specific_to_programme":null
	        },
	        {  
	            "day":2,
	            "time":14,
	            "semester":1,
	            "year":2016,
	            "is_lab_entry":true,
	            "module":{  
	                "code":"CT103",
	                "ects":10,
	                "name":"Programming",
	                "short_name":"",
	                "semester":"all year"
	            },
	            "lecturers":[  
	                {  
	                    "email":"o.molloy@nuigalway.ie",
	                    "name":"Owen Molloy"
	                }
	            ],
	            "venues":[  
	                {  
	                    "code":"IT102",
	                    "name":"IT102"
	                }
	            ],
	            "split":"",
	            "specific_to_programme":"1BCT1"
	        },
	        {  
	            "day":2,
	            "time":15,
	            "semester":1,
	            "year":2016,
	            "is_lab_entry":true,
	            "module":{  
	                "code":"CT103",
	                "ects":10,
	                "name":"Programming",
	                "short_name":"",
	                "semester":"all year"
	            },
	            "lecturers":[  
	                {  
	                    "email":"o.molloy@nuigalway.ie",
	                    "name":"Owen Molloy"
	                }
	            ],
	            "venues":[  
	                {  
	                    "code":"IT102",
	                    "name":"IT102"
	                }
	            ],
	            "split":"",
	            "specific_to_programme":"1BCT1"
	        },
	        {  
	            "day":3,
	            "time":11,
	            "semester":1,
	            "year":2016,
	            "is_lab_entry":false,
	            "module":{  
	                "code":"CT103",
	                "ects":10,
	                "name":"Programming",
	                "short_name":"",
	                "semester":"all year"
	            },
	            "lecturers":[  
	                {  
	                    "email":"o.molloy@nuigalway.ie",
	                    "name":"Owen Molloy"
	                }
	            ],
	            "venues":[  
	                {  
	                    "code":"Charles McMunn Theatre",
	                    "name":"Charles McMunn Theatre"
	                }
	            ],
	            "split":"",
	            "specific_to_programme":null
	        },
	        {  
	            "day":4,
	            "time":11,
	            "semester":1,
	            "year":2016,
	            "is_lab_entry":false,
	            "module":{  
	                "code":"CT1112",
	                "ects":5,
	                "name":"Professional Skills I",
	                "short_name":"",
	                "semester":"2"
	            },
	            "lecturers":[  
	                {  
	                    "email":"e.barrett@nuigalway.ie",
	                    "name":"Enda Barrett"
	                },
	                {  
	                    "email":"j.griffith@nuigalway.ie",
	                    "name":"Josephine Griffith"
	                }
	            ],
	            "venues":[  
	                {  
	                    "code":"AM150",
	                    "name":"Mairtin O Tnuthail Theatre"
	                }
	            ],
	            "split":"",
	            "specific_to_programme":null
	        },
	        {  
	            "day":4,
	            "time":14,
	            "semester":1,
	            "year":2016,
	            "is_lab_entry":false,
	            "module":{  
	                "code":"CT102",
	                "ects":10,
	                "name":"Algorithms & Information Systems",
	                "short_name":"",
	                "semester":"all year"
	            },
	            "lecturers":[  
	                {  
	                    "email":"m.fox@nuigalway.ie",
	                    "name":"Martina Fox"
	                }
	            ],
	            "venues":[  
	                {  
	                    "code":"ENG-2002",
	                    "name":"ENG-2002"
	                }
	            ],
	            "split":"",
	            "specific_to_programme":null
	        },
	        {  
	            "day":4,
	            "time":15,
	            "semester":1,
	            "year":2016,
	            "is_lab_entry":false,
	            "module":{  
	                "code":"CT102",
	                "ects":10,
	                "name":"Algorithms & Information Systems",
	                "short_name":"",
	                "semester":"all year"
	            },
	            "lecturers":[  
	                {  
	                    "email":"j.griffith@nuigalway.ie",
	                    "name":"Josephine Griffith"
	                }
	            ],
	            "venues":[  
	                {  
	                    "code":"ENG-2002",
	                    "name":"ENG-2002"
	                }
	            ],
	            "split":"",
	            "specific_to_programme":null
	        }
	    ]
	};
};

var mockProgrammeTimetables = function() {
	return [
		mockProgrammeTimetable(), {  
		    "year":2016,
		    "semester":1,
		    "version":1,
		    "programme":{  
		        "name":"2BCT1 - BCT1 Bachelor of Science (Computer Science & Information Technology)",
		        "code":"2BCT1"
		    },
		    "date_updated":"2017-01-15T16:11:41.713783Z",
		    "entries":[  

		    ]
		}
	];
};

var mockVenueTimetable = function() {
	return {  
	    "year":2016,
	    "semester":1,
	    "version":1,
	    "venue":{  
	        "code":"IT102",
	        "name":"IT102"
	    },
	    "date_updated":"2017-01-15T16:11:41.713783Z",
	    "entries":[  
	        {  
	            "day":1,
	            "time":17,
	            "semester":1,
	            "year":2016,
	            "is_lab_entry":true,
	            "module":{  
	                "code":"CT101",
	                "ects":10,
	                "name":"Computing Systems",
	                "short_name":"",
	                "semester":"all year"
	            },
	            "lecturers":[  
	                {  
	                    "email":"p.bigioi@nuigalway.ie",
	                    "name":"Petronel Bigioi"
	                }
	            ],
	            "venues":[  
	                {  
	                    "code":"IT102",
	                    "name":"IT102"
	                },
	                {  
	                    "code":"IT106",
	                    "name":"IT106"
	                }
	            ],
	            "split":"",
	            "specific_to_programme":"1BCT1"
	        },
	        {  
	            "day":1,
	            "time":18,
	            "semester":1,
	            "year":2016,
	            "is_lab_entry":true,
	            "module":{  
	                "code":"CT101",
	                "ects":10,
	                "name":"Computing Systems",
	                "short_name":"",
	                "semester":"all year"
	            },
	            "lecturers":[  
	                {  
	                    "email":"p.bigioi@nuigalway.ie",
	                    "name":"Petronel Bigioi"
	                }
	            ],
	            "venues":[  
	                {  
	                    "code":"IT102",
	                    "name":"IT102"
	                },
	                {  
	                    "code":"IT106",
	                    "name":"IT106"
	                }
	            ],
	            "split":"",
	            "specific_to_programme":"1BCT1"
	        },
	        {  
	            "day":2,
	            "time":14,
	            "semester":1,
	            "year":2016,
	            "is_lab_entry":true,
	            "module":{  
	                "code":"CT103",
	                "ects":10,
	                "name":"Programming",
	                "short_name":"",
	                "semester":"all year"
	            },
	            "lecturers":[  
	                {  
	                    "email":"o.molloy@nuigalway.ie",
	                    "name":"Owen Molloy"
	                }
	            ],
	            "venues":[  
	                {  
	                    "code":"IT102",
	                    "name":"IT102"
	                }
	            ],
	            "split":"",
	            "specific_to_programme":"1BCT1"
	        },
	        {  
	            "day":2,
	            "time":15,
	            "semester":1,
	            "year":2016,
	            "is_lab_entry":true,
	            "module":{  
	                "code":"CT103",
	                "ects":10,
	                "name":"Programming",
	                "short_name":"",
	                "semester":"all year"
	            },
	            "lecturers":[  
	                {  
	                    "email":"o.molloy@nuigalway.ie",
	                    "name":"Owen Molloy"
	                }
	            ],
	            "venues":[  
	                {  
	                    "code":"IT102",
	                    "name":"IT102"
	                }
	            ],
	            "split":"",
	            "specific_to_programme":"1BCT1"
	        }
	    ]
	};
};

var mockVenueTimetables = function() {
	return [
		mockVenueTimetable(), {  
		    "year":2016,
		    "semester":1,
		    "version":1,
		    "venue":{  
		        "code":"IT106",
		        "name":"IT106"
		    },
		    "date_updated":"2017-01-15T16:11:41.713783Z",
		    "entries":[  
		        {  
		            "day":1,
		            "time":17,
		            "semester":1,
		            "year":2016,
		            "is_lab_entry":true,
		            "module":{  
		                "code":"CT101",
		                "ects":10,
		                "name":"Computing Systems",
		                "short_name":"",
		                "semester":"all year"
		            },
		            "lecturers":[  
		                {  
		                    "email":"p.bigioi@nuigalway.ie",
		                    "name":"Petronel Bigioi"
		                }
		            ],
		            "venues":[  
		                {  
		                    "code":"IT102",
		                    "name":"IT102"
		                },
		                {  
		                    "code":"IT106",
		                    "name":"IT106"
		                }
		            ],
		            "split":"",
		            "specific_to_programme":"1BCT1"
		        },
		        {  
		            "day":1,
		            "time":18,
		            "semester":1,
		            "year":2016,
		            "is_lab_entry":true,
		            "module":{  
		                "code":"CT101",
		                "ects":10,
		                "name":"Computing Systems",
		                "short_name":"",
		                "semester":"all year"
		            },
		            "lecturers":[  
		                {  
		                    "email":"p.bigioi@nuigalway.ie",
		                    "name":"Petronel Bigioi"
		                }
		            ],
		            "venues":[  
		                {  
		                    "code":"IT102",
		                    "name":"IT102"
		                },
		                {  
		                    "code":"IT106",
		                    "name":"IT106"
		                }
		            ],
		            "split":"",
		            "specific_to_programme":"1BCT1"
		        }
		    ]
		}
	];
};

var mockProgrammeTimetableDisplay = function() {
	return {  
	    "9":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "10":{  
	        "1":{  
	            "code":"EE130",
	            "name":"Fundamentals of EEE:",
	            "venues":"ENG-2001",
	            "lecturers":"(J Breslin)",
	            "isSplit":false,
	            "specificToProgramme":null
	        },
	        "2":{  
	            "A":{  
	                "code":"MA160",
	                "name":"Mathematics:",
	                "venues":"Tyndall Theatre"
	            },
	            "B":{  
	                "code":"MA190",
	                "name":"Mathematics (H):",
	                "venues":"Anderson Theatre"
	            },
	            "isSplit":true
	        },
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "11":{  
	        "1":{  
	            "code":"CT103",
	            "name":"Programming:",
	            "venues":"Dillon Theatre",
	            "lecturers":"(O Molloy)",
	            "isSplit":false,
	            "specificToProgramme":null
	        },
	        "2":null,
	        "3":{  
	            "code":"CT103",
	            "name":"Programming:",
	            "venues":"Charles McMunn Theatre",
	            "lecturers":"(O Molloy)",
	            "isSplit":false,
	            "specificToProgramme":null
	        },
	        "4":{  
	            "code":"CT1112",
	            "name":"Professional Skills I:",
	            "venues":"AM150",
	            "lecturers":"(E Barrett/J Griffith)",
	            "isSplit":false,
	            "specificToProgramme":null
	        },
	        "5":null
	    },
	    "12":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "13":{  
	        "1":{  
	            "A":{  
	                "code":"MA160",
	                "name":"Mathematics:",
	                "venues":"AC213"
	            },
	            "B":{  
	                "code":"MA190",
	                "name":"Mathematics (H):",
	                "venues":"IT250"
	            },
	            "isSplit":true
	        },
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "14":{  
	        "1":{  
	            "code":"CT101",
	            "name":"Computing Systems:",
	            "venues":"AC202",
	            "lecturers":"(P Bigioi)",
	            "isSplit":false,
	            "specificToProgramme":null
	        },
	        "2":{  
	            "code":"CT103",
	            "name":"Programming Lab:",
	            "venues":"IT102",
	            "lecturers":"(O Molloy)",
	            "isSplit":false,
	            "specificToProgramme":"1BCT1"
	        },
	        "3":null,
	        "4":{  
	            "code":"CT102",
	            "name":"Algorithms & Information Systems:",
	            "venues":"ENG-2002",
	            "lecturers":"(M Fox)",
	            "isSplit":false,
	            "specificToProgramme":null
	        },
	        "5":null
	    },
	    "15":{  
	        "1":{  
	            "code":"CT101",
	            "name":"Computing Systems:",
	            "venues":"IT125",
	            "lecturers":"(P Bigioi)",
	            "isSplit":false,
	            "specificToProgramme":null
	        },
	        "2":{  
	            "code":"CT103",
	            "name":"Programming Lab:",
	            "venues":"IT102",
	            "lecturers":"(O Molloy)",
	            "isSplit":false,
	            "specificToProgramme":"1BCT1"
	        },
	        "3":null,
	        "4":{  
	            "code":"CT102",
	            "name":"Algorithms & Information Systems:",
	            "venues":"ENG-2002",
	            "lecturers":"(J Griffith)",
	            "isSplit":false,
	            "specificToProgramme":null
	        },
	        "5":null
	    },
	    "16":{  
	        "1":{  
	            "code":"EE130",
	            "name":"Fundamentals of EEE:",
	            "venues":"ENG-2002",
	            "lecturers":"(J Breslin)",
	            "isSplit":false,
	            "specificToProgramme":null
	        },
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "17":{  
	        "1":{  
	            "code":"CT101",
	            "name":"Computing Systems Lab:",
	            "venues":"IT102/IT106",
	            "lecturers":"(P Bigioi)",
	            "isSplit":false,
	            "specificToProgramme":"1BCT1"
	        },
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "18":{  
	        "1":{  
	            "code":"CT101",
	            "name":"Computing Systems Lab:",
	            "venues":"IT102/IT106",
	            "lecturers":"(P Bigioi)",
	            "isSplit":false,
	            "specificToProgramme":"1BCT1"
	        },
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "19":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    }
	};
};

var mockVenueTimetableDisplay = function() {
	return {  
	    "9":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "10":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "11":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "12":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "13":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "14":{  
	        "1":null,
	        "2":{  
	            "code":"CT103",
	            "name":"Programming Lab:",
	            "venues":"IT102",
	            "lecturers":"(O Molloy)",
	            "isSplit":false,
	            "specificToProgramme":"1BCT1"
	        },
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "15":{  
	        "1":null,
	        "2":{  
	            "code":"CT103",
	            "name":"Programming Lab:",
	            "venues":"IT102",
	            "lecturers":"(O Molloy)",
	            "isSplit":false,
	            "specificToProgramme":"1BCT1"
	        },
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "16":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "17":{  
	        "1":{  
	            "code":"CT101",
	            "name":"Computing Systems Lab:",
	            "venues":"IT102/IT106",
	            "lecturers":"(P Bigioi)",
	            "isSplit":false,
	            "specificToProgramme":"1BCT1"
	        },
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "18":{  
	        "1":{  
	            "code":"CT101",
	            "name":"Computing Systems Lab:",
	            "venues":"IT102/IT106",
	            "lecturers":"(P Bigioi)",
	            "isSplit":false,
	            "specificToProgramme":"1BCT1"
	        },
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "19":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    }
	};
};

// timetable PDF mock data
var mockActiveProgrammeTimetableForPdf = function() {
	return {  
	    "year":2016,
	    "semester":1,
	    "version":1,
	    "programme":{  
	        "name":"1BCT1 - BCT1 Bachelor of Science (Computer Science & Information Technology)",
	        "code":"1BCT1"
	    },
	    "date_updated":"2017-01-15T16:11:41.713783Z",
	    "entries":[  
	        {  
	            "day":1,
	            "time":11,
	            "semester":1,
	            "year":2016,
	            "is_lab_entry":false,
	            "module":{  
	                "code":"CT103",
	                "ects":10,
	                "name":"Programming",
	                "short_name":"",
	                "semester":"all year"
	            },
	            "lecturers":[  
	                {  
	                    "email":"o.molloy@nuigalway.ie",
	                    "name":"Owen Molloy"
	                }
	            ],
	            "venues":[  
	                {  
	                    "code":"Dillon Theatre",
	                    "name":"Dillon Theatre"
	                }
	            ],
	            "split":"",
	            "specific_to_programme":null
	        },
	        {  
	            "day":2,
	            "time":14,
	            "semester":1,
	            "year":2016,
	            "is_lab_entry":true,
	            "module":{  
	                "code":"CT103",
	                "ects":10,
	                "name":"Programming",
	                "short_name":"",
	                "semester":"all year"
	            },
	            "lecturers":[  
	                {  
	                    "email":"o.molloy@nuigalway.ie",
	                    "name":"Owen Molloy"
	                }
	            ],
	            "venues":[  
	                {  
	                    "code":"IT102",
	                    "name":"IT102"
	                }
	            ],
	            "split":"",
	            "specific_to_programme":"1BCT1"
	        }
	    ]
	};
};

var mockActiveVenueTimetableForPdf = function() {
	return {  
	    "year":2016,
	    "semester":1,
	    "version":1,
	    "venue":{  
	        "code":"IT102",
	        "name":"IT102"
	    },
	    "date_updated":"2017-01-15T16:11:41.713783Z",
	    "entries":[  
	        {  
	            "day":2,
	            "time":14,
	            "semester":1,
	            "year":2016,
	            "is_lab_entry":true,
	            "module":{  
	                "code":"CT103",
	                "ects":10,
	                "name":"Programming",
	                "short_name":"",
	                "semester":"all year"
	            },
	            "lecturers":[  
	                {  
	                    "email":"o.molloy@nuigalway.ie",
	                    "name":"Owen Molloy"
	                }
	            ],
	            "venues":[  
	                {  
	                    "code":"IT102",
	                    "name":"IT102"
	                }
	            ],
	            "split":"",
	            "specific_to_programme":"1BCT1"
	        }
	    ]
	};
};

var mockProgrammeTimetableDisplayForPdf = function() {
	return {  
	    "9":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "10":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "11":{  
	        "1":{  
	            "code":"CT103",
	            "name":"Programming:",
	            "venues":"Dillon Theatre",
	            "lecturers":"(O Molloy)",
	            "isSplit":false,
	            "specificToProgramme":null
	        },
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "12":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "13":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "14":{  
	        "1":null,
	        "2":{  
	            "code":"CT103",
	            "name":"Programming Lab:",
	            "venues":"IT102",
	            "lecturers":"(O Molloy)",
	            "isSplit":false,
	            "specificToProgramme":"1BCT1"
	        },
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "15":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "16":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "17":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "18":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "19":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    }
	};
};

var mockVenueTimetableDisplayForPdf = function() {
	return {  
	    "9":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "10":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "11":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "12":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "13":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "14":{  
	        "1":null,
	        "2":{  
	            "code":"CT103",
	            "name":"Programming Lab:",
	            "venues":"IT102",
	            "lecturers":"(O Molloy)",
	            "isSplit":false,
	            "specificToProgramme":"1BCT1"
	        },
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "15":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "16":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "17":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "18":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    },
	    "19":{  
	        "1":null,
	        "2":null,
	        "3":null,
	        "4":null,
	        "5":null
	    }
	};
};