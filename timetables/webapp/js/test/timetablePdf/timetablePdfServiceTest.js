"use strict";

describe("Timetable PDF service", function() {
	beforeEach(module('timetablesApp.timetablePdf'));

	var service;

	var days = {
        1: 'Monday',
        2: 'Tuesday',
        3: 'Wednesday',
        4: 'Thursday',
        5: 'Friday'
    };

    var times = {
        9: '9:00am',
        10: '10:00am',
        11: '11:00am',
        12: '12:00pm',
        13: '1:00pm',
        14: '2:00pm',
        15: '3:00pm',
        16: '4:00pm',
        17: '5:00pm',
        18: '6:00pm',
        19: '7:00pm'
    };

    var year = '2016/17';
    var semester = 1;
    var date = '15/01/2016';

	beforeEach(inject(function (timetablePdfService) {
	    service = timetablePdfService;
  	}));

	it("should generate a heading for programme timetables", function() {	
		var pdf = service.generatePdf(days, times, 'programme', year, semester,
			mockProgrammeTimetableDisplayForPdf(),
			mockActiveProgrammeTimetableForPdf(), date);

		var header = pdf.content[0].table.body[0][0].table.body[0];

		expect(header[0].text).toBe('1BCT1');
		expect(header[2].text).toBe('Timetable 2016/17');
		expect(header[4].text).toBe('Semester 1');
	});

	it("should generate a heading for venue timetables", function() {
		var pdf = service.generatePdf(days, times, 'venue', year, semester,
			mockVenueTimetableDisplayForPdf(),
			mockActiveVenueTimetableForPdf(), date);

		var header = pdf.content[0].table.body[0][0].table.body[0];

		expect(header[0].text).toBe('IT102');
		expect(header[2].text).toBe('Lab Timetable 2016/17');
		expect(header[4].text).toBe('Semester 1');
	});

	it("should generate a footer", function() {
		var pdf = service.generatePdf(days, times, 'programme', year, semester,
			mockProgrammeTimetableDisplayForPdf(),
			mockActiveProgrammeTimetableForPdf(), date);

		var footer = pdf.content[1].table.body[0];

		expect(footer[0].text).toBe('Version 1');
		expect(footer[2].text).toBe('http://www.it.nuigalway.ie/');
		expect(footer[4].text).toBe('15/01/2016');
	});

	it("should generate a row for days of the week", function() {
		var pdf = service.generatePdf(days, times, 'programme', year, semester,
			mockProgrammeTimetableDisplayForPdf(),
			mockActiveProgrammeTimetableForPdf(), date);

		var row = pdf.content[0].table.body[1];

		expect(row[0].text).toBe('Time/Day');
		expect(row[1].text).toBe('Monday');
		expect(row[2].text).toBe('Tuesday');
		expect(row[3].text).toBe('Wednesday');
		expect(row[4].text).toBe('Thursday');
		expect(row[5].text).toBe('Friday');
	});

	it("should generate the entries for a programme", function() {
		var pdf = service.generatePdf(days, times, 'programme', year, semester,
			mockProgrammeTimetableDisplayForPdf(),
			mockActiveProgrammeTimetableForPdf(), date);

		var table = pdf.content[0].table.body;

		expect(table[4][1].text)
			.toBe('CT103 Programming:\nDillon Theatre\n(O Molloy)');
		expect(table[7][2].text)
			.toBe('CT103 Programming Lab:\nIT102\n(O Molloy)');
	});

	it("should generate the entries for a venue", function() {
		var pdf = service.generatePdf(days, times, 'venue', year, semester,
			mockVenueTimetableDisplayForPdf(),
			mockActiveVenueTimetableForPdf(), date);

		var table = pdf.content[0].table.body;

		expect(table[7][2].text)
			.toBe('CT103 Programming Lab:\n(O Molloy)\n');
	});
});