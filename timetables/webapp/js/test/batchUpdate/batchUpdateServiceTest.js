"use strict";

describe("Batch update service", function() {
	beforeEach(module('timetablesApp.batchUpdate'));

	var service;
	var httpBackend;

	var year = 2016;
	var code = '1BCT1';

	beforeEach(inject(function (batchUpdateService, $httpBackend) {
	    service = batchUpdateService;
	    httpBackend = $httpBackend;

	    httpBackend
	    	.when('GET', 'api/batchupdate/2016/1BCT1/')
	    	.respond(200, mockBatchUpdateStatus());
  	}));

	it("should call the correct url", function() {	
	    httpBackend.expectGET('api/batchupdate/' + year + '/' + code + '/');

		service.updateProgramme(year, code);

		httpBackend.flush();
	});

	it("should return data", function() {
		var response; 

		service.updateProgramme(year, code)
			.then(function(data) {
				response = data;
			}
		);

		httpBackend.flush();

		expect(angular.equals(response, mockBatchUpdateStatus())).toBeTruthy();
	});

});