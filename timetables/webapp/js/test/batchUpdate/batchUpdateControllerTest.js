"use strict";

describe('BatchUpdateController', function() {
	var q;
	var httpBackend;
	var ctrl;
	var scope;
	var programmes;

	beforeEach(module('timetablesApp.batchUpdate'));

	// mock services
	var mockProgrammeService = {
		getAllProgrammes: function() {
			var deferred = q.defer();
			deferred.resolve(mockProgrammes());
	    	return deferred.promise;
		},

	    getProgramme: function(code) {
	    	var deferred = q.defer();

	    	if (code === '3BCT1') {
	    		deferred.resolve(mockNewProgramme());
	    	} else {
	    		deferred.resolve(mockProgramme());
	    	}

	    	return deferred.promise;
	    }
	};

	var mockBatchUpdateService = {
		updateProgramme: function(year, code) {
			var deferred = q.defer();
			deferred.resolve(mockBatchUpdateStatus());
	    	return deferred.promise;
		}	
	};

	// providers for mocked services
	beforeEach(function() {
		module(function($provide) {
			$provide.value('programmeService', mockProgrammeService)
		});

		module(function($provide) {
			$provide.value('batchUpdateService', mockBatchUpdateService)
		});
	});

	// set up test controller and mock promises/http
	beforeEach(inject(function($q, $rootScope, $httpBackend, $controller) {
		q = $q;
		httpBackend = $httpBackend;

		scope = $rootScope.$new();
		ctrl = $controller('BatchUpdateController', {
			$scope: scope
		});

		scope.$apply(); // resolve mocked promises

		// create fake data for programmes with initialised status
		programmes = mockProgrammes();

		for (var i in programmes) {
			programmes[i].updateStatus = 'idle';
		}
	}));

	// tests
	it('should get the current year', function() {
		var year = new Date().getFullYear();

		expect(ctrl.currentYear).toBe(year);
		expect(ctrl.selectedYear).toBe(String(year));
	});

	it('should initialise new programme status as "idle"', function() {
		expect(ctrl.newProgrammeStatus).toBe('idle');
	})

	it('should populate the years" dropdown', function() {
		var year = new Date().getFullYear();

		var years = [{
            'name': year + '/' + (year + 1),
            'id': String(year)
        }];

        for (var i=1; i<5; i++) {
            years.push({
                'name': (year - i) + '/' + (year - i + 1),
                'id': String(year - i)
            });
        }

        expect(angular.equals(ctrl.years, years)).toBeTruthy();
	});

	it('should find existing programme data and initialise its update status', function() {
		expect(angular.equals(ctrl.programmes, programmes)).toBeTruthy();
	});

	it('should update a programmes status when it is updating', function() {
		ctrl.updateProgramme(ctrl.programmes[0]);
		expect(ctrl.programmes[0].updateStatus).toBe('updating');

		scope.$apply();
		expect(ctrl.programmes[0].updateStatus).toBe('Success');
	});

	it('should update all programmes', function() {
		ctrl.updateAllProgrammes();

		for (var i in ctrl.programmes) {
			expect(ctrl.programmes[i].updateStatus).toBe('updating');
		}

		scope.$apply();

		for (var i in programmes) {
			expect(ctrl.programmes[i].updateStatus).toBe('Success');
		}
	});

	it('should check to see if a programme already exists before adding it', function() {
		ctrl.newProgramme = '1BCT1';
		ctrl.addNewProgramme('1BCT1');

		scope.$apply();

		expect(ctrl.programmes.length).toBe(2);
	});

	it('should add a new programme to the list', function() {
		ctrl.newProgramme = '3BCT1';
		ctrl.addNewProgramme('3BCT1');

		scope.$apply();

		expect(ctrl.programmes.length).toBe(3);
		expect(ctrl.programmes[2].code).toBe('3BCT1');
	});

	it('should provide status messages when adding a new programme', function() {
		expect(ctrl.newProgrammeStatus).toBe("idle");

		ctrl.newProgramme = '1BCT1';
		ctrl.addNewProgramme('1BCT1');
		scope.$apply();

		expect(ctrl.newProgrammeStatus).toBe("Programme already exists");

		ctrl.newProgramme = '3BCT1';
		ctrl.addNewProgramme('3BCT1');

		expect(ctrl.newProgrammeStatus).toBe("updating");

		scope.$apply();

		expect(ctrl.newProgrammeStatus).toBe("Success");
	});

	it('should reset the new programme status if user input is changed', function() {
		ctrl.newProgrammeStatus = 'idle';
		ctrl.addProgrammeInputChanged();

		// nothing to change
		expect(ctrl.newProgrammeStatus).toBe('idle');

		ctrl.newProgrammeStatus = 'updating';
		ctrl.addProgrammeInputChanged();

		// dont reset if its updating
		expect(ctrl.newProgrammeStatus).toBe('updating');

		ctrl.newProgrammeStatus = 'Success';
		ctrl.addProgrammeInputChanged();

		// reset if its finished updating
		expect(ctrl.newProgrammeStatus).toBe('idle');
	});

	it('should decide whether or not to show programme update statuses', function() {
		ctrl.programmes[0].updateStatus = 'idle';
		expect(ctrl.showProgrammeUpdateStatus(ctrl.programmes[0]))
				.toBeFalsy();

		ctrl.programmes[0].updateStatus = 'updating';
		expect(ctrl.showProgrammeUpdateStatus(ctrl.programmes[0]))
				.toBeFalsy();

		ctrl.programmes[0].updateStatus = 'Success';
		expect(ctrl.showProgrammeUpdateStatus(ctrl.programmes[0]))
				.toBeTruthy();
	});

	it('should decide whether or not to show new programme update statuses', function() {
		ctrl.newProgrammeStatus = 'idle';
		expect(ctrl.showNewProgrammeStatus()).toBeFalsy();

		ctrl.newProgrammeStatus = 'updating';
		expect(ctrl.showNewProgrammeStatus()).toBeFalsy();

		ctrl.newProgrammeStatus = 'Success';
		expect(ctrl.showNewProgrammeStatus()).toBeTruthy();
	});

	it('should decide whether or not to show add programme button', function() {
        expect(ctrl.showAddNewProgrammeButton()).toBeFalsy();

        ctrl.newProgramme = '3BCT1';
        expect(ctrl.showAddNewProgrammeButton()).toBeTruthy();

        ctrl.newProgrammeStatus = 'updating';
        expect(ctrl.showAddNewProgrammeButton()).toBeFalsy();

        ctrl.newProgrammeStatus = 'Success';
        expect(ctrl.showAddNewProgrammeButton()).toBeFalsy();
	});

})