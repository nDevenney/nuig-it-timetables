"use strict";

describe('TimetableController', function() {
	var q;
	var httpBackend;
	var ctrl;
	var scope;

	var stateParams = {
		type: 'programme'
	};

	var activeSemester = {
		semester: 1,
		year: 2016
	};

 	var days = {
        1: 'Monday',
        2: 'Tuesday',
        3: 'Wednesday',
        4: 'Thursday',
        5: 'Friday'
    };

    var times = {
        9: '9:00am',
        10: '10:00am',
        11: '11:00am',
        12: '12:00pm',
        13: '1:00pm',
        14: '2:00pm',
        15: '3:00pm',
        16: '4:00pm',
        17: '5:00pm',
        18: '6:00pm',
        19: '7:00pm'
    };

	beforeEach(module('timetablesApp.timetable'));

	// mock services
	var mockProgrammeService = {
		getAllProgrammes: function() {
			var deferred = q.defer();
			deferred.resolve(mockProgrammes());
	    	return deferred.promise;
		}
	};

	var mockTimetableService = {
		getAllTimetables: function() {
			var deferred = q.defer();
			deferred.resolve(mockProgrammeTimetables());
	    	return deferred.promise;	
		},

		getTimetable: function(code, type, year, semester) {
			var deferred = q.defer();

			if (code === '1BCT1') {
				deferred.resolve(mockProgrammeTimetable());
			} else {
				// 2BCT
				deferred.resolve(mockProgrammeTimetables()[1]);
			}
	    	return deferred.promise;	
		},

		initTimetableDisplay: function(data) {
			return mockProgrammeTimetableDisplay();
		},

		setDisplayDate: function() {
			return "15/01/2017";
		}
	};

	var mockTimetablePdfService = {};

	// providers for mocked services
	beforeEach(function() {
		module(function($provide) {
			$provide.value('programmeService', mockProgrammeService);
		});

		module(function($provide) {
			$provide.value('timetableService', mockTimetableService);
		});

		module(function($provide) {
			$provide.value('timetablePdfService', mockTimetablePdfService);
		});
	});

	// set up test controller and mock promises/http
	beforeEach(inject(function($q, $rootScope, $httpBackend, $controller) {
		q = $q;
		httpBackend = $httpBackend;
		scope = $rootScope.$new();

		ctrl = $controller('TimetableController', {
			$scope: scope,
			$stateParams: stateParams,
			activeSemester: activeSemester
		});

		scope.$apply(); // resolve mocked promises
	}));

	// tests
	it('should set the current semester', function() {
		expect(ctrl.semester).toBe(activeSemester.semester);
	});

	it('should set type', function() {
		expect(ctrl.type).toBe(stateParams.type);
	});

	it('should set display year', function() {
		expect(ctrl.displayYear).toBe("2016/2017");
	});

	it('should set days', function() {
		expect(angular.equals(ctrl.days, days)).toBeTruthy();
	});

	it('should set times', function() {
		expect(angular.equals(ctrl.times, times)).toBeTruthy();
	});

	it('should get all timetables from a service', function() {
		expect(angular.equals(ctrl.allTimetables, mockProgrammeTimetables()))
			.toBeTruthy();
	});

	it('should set active timetable as the first timetable returned by the service', function() {
		expect(angular.equals(ctrl.activeTimetable, mockProgrammeTimetable()))
			.toBeTruthy();
	});

	it('should set display date from a service', function() {
		expect(ctrl.displayDate).toBe('15/01/2017');
	});

	it('should get active timetable display from a service', function() {
		expect(angular.equals(ctrl.timetableDisplay, mockProgrammeTimetableDisplay()))
			.toBeTruthy();
	});

	it('should return the object keys for timetableDisplay', function() {
		var keys = ctrl.timetableDisplayKeys();

		var expected = ['9', '10', '11', '12', '13', '14', '15', '16',
		                '17', '18', '19'];

		expect(angular.equals(keys, expected)).toBeTruthy();
	});

	it('should decide if a timetable is active or not', function() {
		expect(ctrl.isTimetableActive(mockProgrammeTimetable()))
			.toBeTruthy();

		// 2BCT
		expect(ctrl.isTimetableActive(mockProgrammeTimetables()[1]))
			.toBeFalsy();
	});

})