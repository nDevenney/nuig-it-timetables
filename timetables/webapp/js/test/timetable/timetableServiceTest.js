"use strict";

describe("Timetable service", function() {
	beforeEach(module('timetablesApp.timetable'));

	var service;
	var httpBackend;

	var year = 2016;
	var semester = 1;
	var programmeCode = '1BCT1';
	var venueCode = 'IT102';

    var days = {
        1: 'Monday',
        2: 'Tuesday',
        3: 'Wednesday',
        4: 'Thursday',
        5: 'Friday'
    };

    var times = {
        9: '9:00am',
        10: '10:00am',
        11: '11:00am',
        12: '12:00pm',
        13: '1:00pm',
        14: '2:00pm',
        15: '3:00pm',
        16: '4:00pm',
        17: '5:00pm',
        18: '6:00pm',
        19: '7:00pm'
    };

	beforeEach(inject(function (timetableService, $httpBackend) {
	    service = timetableService;
	    httpBackend = $httpBackend;

	    httpBackend
	    	.when('GET', 'api/timetable/2016/1/programme/')
	    	.respond(200, mockProgrammeTimetables()
	    );

	    httpBackend
	    	.when('GET', 'api/timetable/2016/1/programme/1BCT1/')
	    	.respond(200, mockProgrammeTimetable()
	    );

	   	httpBackend
	    	.when('GET', 'api/timetable/2016/1/venue/')
	    	.respond(200, mockVenueTimetables()
	    );

	   	httpBackend
	    	.when('GET', 'api/timetable/2016/1/venue/IT102/')
	    	.respond(200, mockVenueTimetable()
	    );

  	}));

	it("should call the correct url for a programme timetable", function() {	
	    httpBackend.expectGET('api/timetable/2016/1/programme/' + 
	    	programmeCode + '/');

		service.getTimetable(programmeCode, 'programme', year, semester);

		httpBackend.flush();
	});

	it("should return data for a programme timetable", function() {
		var response; 

		service.getTimetable(programmeCode, 'programme', year, semester)
			.then(function(data) {
				response = data;
			}
		);

		httpBackend.flush();

		expect(angular.equals(response, mockProgrammeTimetable()))
			.toBeTruthy();
	});

	it("should call the correct url for all programme timetables", function() {	
	    httpBackend.expectGET('api/timetable/2016/1/programme/');

		service.getAllTimetables('programme', year, semester);

		httpBackend.flush();
	});

	it("should return data for all  programme timetables", function() {
		var response; 

		service.getAllTimetables('programme', year, semester)
			.then(function(data) {
				response = data;
			}
		);

		httpBackend.flush();

		expect(angular.equals(response, mockProgrammeTimetables()))
			.toBeTruthy();
	});

	it("should call the correct url for a venue timetable", function() {	
	    httpBackend.expectGET('api/timetable/2016/1/venue/' + 
	    	venueCode + '/');

		service.getTimetable(venueCode, 'venue', year, semester);

		httpBackend.flush();
	});

	it("should return data for a venue timetable", function() {
		var response; 

		service.getTimetable(venueCode, 'venue', year, semester)
			.then(function(data) {
				response = data;
			}
		);

		httpBackend.flush();

		expect(angular.equals(response, mockVenueTimetable()))
			.toBeTruthy();
	});

	it("should call the correct url for all venue timetables", function() {	
	    httpBackend.expectGET('api/timetable/2016/1/venue/');

		service.getAllTimetables('venue', year, semester);

		httpBackend.flush();
	});

	it("should return data for venue timetables", function() {
		var response; 

		service.getAllTimetables('venue', year, semester)
			.then(function(data) {
				response = data;
			}
		);

		httpBackend.flush();

		expect(angular.equals(response, mockVenueTimetables()))
			.toBeTruthy();
	});

	it("should set display date", function() {
		var date = "2017-01-15T16:11:41.713783Z";

		var displayDate = service.setDisplayDate(date);

		expect(displayDate).toBe("15/01/2017");
	});

	describe("initTimetableDisplay", function() {
		it("should set empty entries to null", function() {
			var display = service
				.initTimetableDisplay(mockProgrammeTimetable(), times, days);

			expect(display[9][1]).toBeNull();
		});

		it("should handle regular entries for programmes", function() {
			var display = service
				.initTimetableDisplay(mockProgrammeTimetable(), times, days);

			expect(angular.equals(display[10][1],
				mockProgrammeTimetableDisplay()[10][1])).toBeTruthy();
		});

		it("should handle regular entries for venues", function() {
			var display = service
				.initTimetableDisplay(mockVenueTimetable(), times, days);

			expect(angular.equals(display[14][2],
				mockVenueTimetableDisplay()[14][2])).toBeTruthy();
		});

		it("should handle split entries for programmes", function() {
			var display = service
				.initTimetableDisplay(mockProgrammeTimetable(), times, days);

			expect(angular.equals(display[10][2],
				mockProgrammeTimetableDisplay()[10][2])).toBeTruthy();
		});

		it("should handle lab entries for programmes", function() {
			var display = service
				.initTimetableDisplay(mockProgrammeTimetable(), times, days);

			expect(angular.equals(display[14][2],
				mockProgrammeTimetableDisplay()[14][2])).toBeTruthy();
		});

		it("should handle entries with multiple lecturers", function() {
			var display = service
				.initTimetableDisplay(mockProgrammeTimetable(), times, days);

			expect(angular.equals(display[11][4],
				mockProgrammeTimetableDisplay()[11][4])).toBeTruthy();
		});

		it("should handle entries with multiple venues", function() {
			var display = service
				.initTimetableDisplay(mockProgrammeTimetable(), times, days);

			expect(angular.equals(display[17][1],
				mockProgrammeTimetableDisplay()[17][1])).toBeTruthy();
		});
	});
});