"use strict";

describe("Programme service", function() {
	beforeEach(module('timetablesApp.programme'));

	var service;
	var httpBackend;

	var code = '1BCT1';

	beforeEach(inject(function (programmeService, $httpBackend) {
	    service = programmeService;
	    httpBackend = $httpBackend;

	    httpBackend
	    	.when('GET', 'api/programme/')
	    	.respond(200, mockProgrammes()
	    );

	    httpBackend
	    	.when('GET', 'api/programme/1BCT1/')
	    	.respond(200, mockProgramme()
	    );	
  	}));

	it("should call the correct url for programmes", function() {	
	    httpBackend.expectGET('api/programme/');

		service.getAllProgrammes();

		httpBackend.flush();
	});

	it("should return data for programmes", function() {
		var response; 

		service.getAllProgrammes()
			.then(function(data) {
				response = data;
			}
		);

		httpBackend.flush();

		expect(angular.equals(response, mockProgrammes())).toBeTruthy();
	});

	it("should call the correct url for a programme", function() {	
	    httpBackend.expectGET('api/programme/' + code + '/');

		service.getProgramme(code);

		httpBackend.flush();
	});

	it("should return data for a programme", function() {
		var response; 

		service.getProgramme(code)
			.then(function(data) {
				response = data;
			}
		);

		httpBackend.flush();

		expect(angular.equals(response, mockProgramme())).toBeTruthy();
	});
});