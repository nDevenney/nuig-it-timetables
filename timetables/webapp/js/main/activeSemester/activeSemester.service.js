(function() {
    'use strict';
    
    angular
        .module('timetablesApp.activeSemester')
        .factory('activeSemesterService', activeSemesterService);

    activeSemesterService.$inject = ['$http'];

    function activeSemesterService($http) {
        return {
            getActiveSemester: function() {
                var url = 'api/activesemester/';

                return $http.get(url)
                    .then(function(response) {
                        return response.data;
                    }
                );
            },
            updateActiveSemester: function(year, semester) {
            	var url = 'api/activesemester/';

                var request = {
                    method: 'PUT',
                    url: url,
                    headers: {
                        'X-CSRFToken': Cookies.get('csrftoken')
                    },
                    data: {
                        'semester': semester,
                        'year': year
                    }
                };

            	return $http(request)
                    .then(function(response) {
            		    return response.data;
            	    }
                );
       		}
    	}
    }
})();