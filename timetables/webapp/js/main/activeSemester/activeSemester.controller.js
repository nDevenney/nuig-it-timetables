(function() {
    'use strict';

    angular
        .module('timetablesApp.activeSemester')
        .controller('ActiveSemesterController', ActiveSemesterController);

    ActiveSemesterController.$inject = ['$scope', 'activeSemesterService'];

    function ActiveSemesterController($scope, activeSemesterService) {
        var vm = this;

        vm.semesters = ["1", "2"];
        vm.selectedSemester = "1";

        vm.currentYear = new Date().getFullYear();
        vm.selectedYear = String(vm.currentYear);

        // populate 'years' dropdown
        vm.years = [{
            'name': vm.currentYear + '/' + (vm.currentYear + 1),
            'id': String(vm.currentYear)
        }];

        for (var i=1; i<5; i++) {
            vm.years.push({
                'name': (vm.currentYear - i) + '/' + (vm.currentYear - i + 1),
                'id': String(vm.currentYear - i)
            });
        }

        activeSemesterService.getActiveSemester()
            .then(function(data) {
                vm.activeSemester = data;

                if (vm.activeSemester.semester === null) {
                    return;
                }

                vm.activeSemester.year = String(vm.activeSemester.year)
                    + '/' + String(vm.activeSemester.year + 1);
            }
        );

        vm.updateActiveSemester = function() {
            activeSemesterService.updateActiveSemester(vm.selectedYear, vm.selectedSemester)
                .then(function(data) {
                    vm.activeSemester = data;

                    vm.activeSemester.year = String(vm.activeSemester.year)
                        + '/' + String(vm.activeSemester.year + 1);
                }
            );
        };
    };
})();
