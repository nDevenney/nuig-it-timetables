(function() {
	'use strict';

	angular
		.module('timetablesApp.timetable', [
			'timetablesApp.programme',
			'timetablesApp.timetablePdf',
			'timetablesApp.activeSemester',
			'ui.router'
		]);
})();