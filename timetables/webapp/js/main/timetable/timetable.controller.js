(function() {
    'use strict';

    angular
        .module('timetablesApp.timetable')
        .controller('TimetableController', TimetableController);

    TimetableController.$inject = ['$scope', '$stateParams', 'timetableService',
            'programmeService', 'activeSemester', 'timetablePdfService',
            'activeSemesterService', '$state'];

    function TimetableController($scope, $stateParams, timetableService,
            programmeService, activeSemester, timetablePdfService,
            activeSemesterService, $state) {
        var vm = this;

        var year = activeSemester.year;
        vm.semester = activeSemester.semester;

        // programme or venue
        vm.type = $stateParams.type;

        vm.displayYear = year + '/' + (parseInt(year) + 1)

        vm.days = {
            1: 'Monday',
            2: 'Tuesday',
            3: 'Wednesday',
            4: 'Thursday',
            5: 'Friday'
        };

        vm.times = {
            9: '9:00am',
            10: '10:00am',
            11: '11:00am',
            12: '12:00pm',
            13: '1:00pm',
            14: '2:00pm',
            15: '3:00pm',
            16: '4:00pm',
            17: '5:00pm',
            18: '6:00pm',
            19: '7:00pm'
        };

        timetableService.getAllTimetables(vm.type, year, vm.semester)
            .then(function(data) {
                if (data.length === 0 || vm.semester === null) {
                    vm.noTimetables = true;
                    return;
                }

                vm.allTimetables = data;

                if ($stateParams.code) {
                    // programme/venue code specified in URL
                    for (var i in vm.allTimetables) {
                        if (vm.allTimetables[i].programme.code === $stateParams.code) {
                            vm.activeTimetable = vm.allTimetables[i];
                        }
                    }
                } else {
                    // set active timetable as the first one
                    vm.activeTimetable = vm.allTimetables[0];
                }

                setDisplayDate();

                getTimetable(vm.activeTimetable[vm.type].code, vm.type,
                    year, vm.semester);
            }
        );

        vm.timetableDisplayKeys = function() {
            if (vm.timetableDisplay) {
                return Object.keys(vm.timetableDisplay);
            }
        };

        vm.isTimetableActive = function(timetable) {
            return timetable[vm.type].code === vm.activeTimetable[vm.type].code;
        };

        vm.changeTimetable = function(timetable) {
            vm.activeTimetable = timetable;

            getTimetable(vm.activeTimetable[vm.type].code, vm.type,
                    year, vm.semester);

            // update URL but don't reload page
            $state.go('timetableSpecific', {type: vm.type,
                code: vm.activeTimetable[vm.type].code}, {notify: false}); 

            setDisplayDate();
        };

        vm.generatePdf = function() {
            var pdf = timetablePdfService.generatePdf(vm.days, vm.times, vm.type,
                    vm.displayYear, vm.semester, vm.timetableDisplay,
                    vm.activeTimetable, vm.displayDate);

            // open PDF in a new tab
            createPdf(pdf).open();
        };

        function setDisplayDate() {
            vm.displayDate = timetableService.setDisplayDate(vm.activeTimetable.date_updated);
        }

        function getTimetable(code, type, year, semester) {
            timetableService.getTimetable(code, type, year, semester)
                .then(function(data) {
                    vm.timetableData = data;
                    vm.timetableDisplay = timetableService
                            .initTimetableDisplay(vm.timetableData, vm.times,
                                                  vm.days);
                }
            );
        }
    };
})();
