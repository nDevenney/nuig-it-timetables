(function() {
    'use strict';
    
    angular
        .module('timetablesApp.timetable')
        .factory('timetableService', timetableService);

    timetableService.$inject = ['$http'];

    function timetableService($http) {
        return {
            getTimetable: function(code, type, year, semester) {
                var url = 'api/timetable/' + year + '/' + semester + 
                    '/' + type + '/' + code + '/';

                return $http.get(url)
                    .then(function(response) {
                        return response.data;
                });
            },

            getAllTimetables: function(type, year, semester) {
                var url = 'api/timetable/' + year + '/' + semester + 
                    '/' + type + '/';

                return $http.get(url)
                    .then(function(response) {
                        return response.data;
                });
            },

            addTimetable: function(type, code, isPublic, year, semester) {
                var url = 'api/timetable/' + type + '/';

                var isPublic = isPublic === 'true' ? true : false;

                var request = {
                    method: 'POST',
                    url: url,
                    headers: {
                        'X-CSRFToken': Cookies.get('csrftoken')
                    },
                    data: {
                        'is_public': isPublic,
                        'code': code,
                        'year': year,
                        'semester': semester
                    }
                };

                return $http(request)
                    .then(function(response) {
                        return response.data;
                    }
                );
            },

            updateTimetable: function(type, timetable) {
                var url = 'api/timetable/' + timetable.year + '/' + 
                    timetable.semester + '/' + type + '/' + timetable[type].code
                    + '/';

                var isPublic = timetable.isPublic === 'true' ? true : false;

                var request = {
                    method: 'PUT',
                    url: url,
                    headers: {
                        'X-CSRFToken': Cookies.get('csrftoken')
                    },
                    data: {
                        'is_public': isPublic
                    }
                };

                return $http(request)
                    .then(function(response) {
                        return response.data;
                    }
                );
            },

            deleteTimetable: function(type, timetable) {
                var url = 'api/timetable/' + timetable.year + '/' + 
                    timetable.semester + '/' + type + '/' + timetable[type].code
                    + '/';

                var request = {
                    method: 'DELETE',
                    url: url,
                    headers: {
                        'X-CSRFToken': Cookies.get('csrftoken')
                    }
                };

                return $http(request)
                    .then(function(response) {
                        return response.data;
                    }
                );
            },

            copyPreviousYear: function(timetable) {
                var url = 'api/timetable/' + timetable.year + '/' + timetable.semester + 
                    '/programme/' + timetable.programme.code + '/copy/';

                var request = {
                    method: 'POST',
                    url: url,
                    headers: {
                        'X-CSRFToken': Cookies.get('csrftoken')
                    }
                };

                return $http(request)
                    .then(function(response) {
                        return response.data;
                    }
                );
            },

            setDisplayDate: function(date) {
                var split = date.split("T");
                split = split[0].split("-");

                return split[2] + "/" + split[1] + "/" + split[0];
            },

            initTimetableDisplay: function(data, times, days) {
                var timetableDisplay = {};

                for (var i in times) {
                    timetableDisplay[i] = {};

                    for (var j in days) {
                        timetableDisplay[i][j] = null;
                    }
                }

                for (var i in data.entries) {
                    var entry = data.entries[i]

                    if(entry.split) {
                        if (!timetableDisplay[entry.time][entry.day]) {
                            timetableDisplay[entry.time][entry.day] = {
                                A: {},
                                B: {},
                                isSplit: true
                            };
                        }

                        timetableDisplay[entry.time][entry.day][entry.split] = {
                            code: entry.module.code,
                            name: getModuleNameDisplay(entry),
                            venues: getVenuesDisplay(entry.venues)
                        };
                    } else {
                        timetableDisplay[entry.time][entry.day] = {
                            code: entry.module.code,
                            name: getModuleNameDisplay(entry),
                            venues: getVenuesDisplay(entry.venues),
                            lecturers: getLecturersDisplay(entry.lecturers),
                            isSplit: false,
                            specificToProgramme: entry.specific_to_programme
                        };
                    }
                }

                return timetableDisplay;
            }
        }

        // private functions
        function getModuleNameDisplay(entry) {
            var name;

            if (entry.module.short_name) {
                name = entry.module.short_name;
            } else {
                name = entry.module.name;
            }

            if (entry.is_lab_entry) {
                return name + " Lab:";
            } else {
                return name + ":";
            }
        }

        function getVenuesDisplay(venues) {
            var display = '';

            for (var i in venues) {
                if (parseInt(i) === venues.length - 1) {
                    display += venues[i].name;
                } else {
                    display += venues[i].name + '/';
                }
            }

            return display;
        }

        function getLecturersDisplay(lecturers) {
            if (lecturers.length === 0) {
                return '';
            }

            var display = '(';

            for (var i in lecturers) {
                var split = lecturers[i].name.split(" ");
                var firstInitial = split[0].split("")[0];

                if (parseInt(i) === lecturers.length - 1) {
                    display += firstInitial + " " + split[1];
                } else {
                    display += firstInitial + " " + split[1] + '/';
                }
            }

            return display + ')';     
        }
    }
})();