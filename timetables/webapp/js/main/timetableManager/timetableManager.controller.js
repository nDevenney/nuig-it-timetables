(function() {
    'use strict';

    angular
        .module('timetablesApp.timetableManager')
        .controller('TimetableManagerController', TimetableManagerController);

    TimetableManagerController.$inject = ['$scope', 'activeSemesterService',
        'timetableService', 'programmeService', 'venueService'];

    function TimetableManagerController($scope, activeSemesterService,
            timetableService, programmeService, venueService) {
        var vm = this;

        // tabs
        vm.programmesActive = true;
        vm.venuesActive = false;

        vm.semesters = ["1", "2"];    
        vm.currentYear = new Date().getFullYear();

        // populate 'years' dropdown
        vm.years = [{
            'name': vm.currentYear + '/' + (vm.currentYear + 1),
            'id': String(vm.currentYear)
        }];

        for (var i=1; i<5; i++) {
            vm.years.push({
                'name': (vm.currentYear - i) + '/' + (vm.currentYear - i + 1),
                'id': String(vm.currentYear - i)
            });
        }

        vm.publicOptions = [{ 'name': "yes", 'id': "true" },
                            { 'name': "no", 'id': "false" }];

        vm.success = null;
        vm.error = null;

        vm.newProgrammeTimetable = {
            'programme': null,
            'isPublic': "false"
        };

        vm.newVenueTimetable = {
            'venue': null,
            'isPublic': "false"
        };

        activeSemesterService.getActiveSemester()
            .then(function(data) {
                vm.activeSemester = data;
                vm.selectedSemester = String(data.semester)
                vm.selectedYear = String(data.year);

                vm.getTimetables();
            }
        );

        programmeService.getAllProgrammes()
            .then(function(data) {
                vm.programmes = data;
                vm.newProgrammeTimetable.programme = data[0].code;
            }
        );

        venueService.getAllVenues()
            .then(function(data) {
                vm.venues = data;
                vm.newVenueTimetable.venue = data[0].code;
                vm.setVenueList();
            }
        );

        vm.copyPreviousYear = function(timetable) {
            timetableService.copyPreviousYear(timetable).
                then(function(data) {
                    console.log(data);
                }
            );
        };

        vm.toggleActive = function(type) {
            if (type === 'programme') {
                vm.programmesActive = true;
                vm.venuesActive = false;
            } else if (type === 'venue') {
                vm.programmesActive = false;
                vm.venuesActive = true;
            }
        };

        vm.addTimetable = function(type) {
            if (type === 'programme') {
                timetableService.addTimetable('programme',
                        vm.newProgrammeTimetable.programme,
                        vm.newProgrammeTimetable.isPublic, vm.selectedYear,
                        vm.selectedSemester)
                    .then(function(data) {
                        success(vm.newProgrammeTimetable.programme + ' timetable created');

                        data.date = timetableService.setDisplayDate(data.date_updated);
                        data.isPublic = String(data.is_public);

                        vm.programmeTimetables.push(data);
                    }, function(data) {
                        error("Error creating timetable");
                    }
                );
            } else if (type === 'venue') {
                timetableService.addTimetable('venue',
                        vm.newVenueTimetable.venue,
                        vm.newVenueTimetable.isPublic, vm.selectedYear,
                        vm.selectedSemester)
                    .then(function(data) {
                        success(vm.newVenueTimetable.venue + ' timetable created');

                        data.date = timetableService.setDisplayDate(data.date_updated);
                        data.isPublic = String(data.is_public);

                        vm.venueTimetables.push(data);

                        vm.setVenueList();
                    }, function(data) {
                        error("Error creating timetable");
                    }
                );
            }
        };

        vm.setVisibility = function(type, timetable) {
            timetableService.updateTimetable(type, timetable)
                .then(function(data) {
                    var visibility = timetable.isPublic == 'true' ? "public" 
                        : "private";

                    success(timetable[type].code + " timetable set to " 
                        + visibility);
                }, function(data) {
                    error("Error updating timetable");
                }
            );
        };

        vm.setAllToPublic = function(type) {
            if (type === 'programme') {
                angular.forEach(vm.programmeTimetables, function(timetable) {
                    timetable.isPublic = "true";

                    timetableService.updateTimetable('programme', timetable)
                        .then(function(data) {}, function(data) {
                            error("Error updating timetables");
                            return;
                        }
                    );
                });
            } else if (type === 'venue') {
                angular.forEach(vm.venueTimetables, function(timetable) {
                    timetable.isPublic = "true";

                    timetableService.updateTimetable('venue', timetable)
                        .then(function(data) {}, function(data) {
                            error("Error updating timetables");
                            return;
                        }
                    );
                });
            }

            success('All ' + type + ' timetables public');
        };

        vm.deleteTimetable = function(type, timetable) {
            timetableService.deleteTimetable(type, timetable)
                .then(function(data) {
                    success(timetable[type].code + " timetable deleted");

                    if (type === 'programme') {
                        var idx = vm.programmeTimetables.indexOf(timetable);
                        vm.programmeTimetables.splice(idx, 1);
                    } else if (type === 'venue') {
                        var idx = vm.venueTimetables.indexOf(timetable);
                        vm.venueTimetables.splice(idx, 1);

                        vm.venues.push(timetable.venue);
                        vm.setVenueList();
                    }
                }, function(data) {
                    error("Error deleting timetable");
                }
            );
        };

        vm.getTimetables = function() {
            getProgrammeTimetables();
            getVenueTimetables();
        };

        vm.setVenueList = function() {
            var list = vm.venues;
            var toRemove = [];

            for (var i=0; i<list.length; i++) {
                if (vm.venueTimetables) {
                    for (var j=0; j<vm.venueTimetables.length; j++) {
                        if (vm.venueTimetables[j].venue.code === list[i].code) {
                            toRemove.push(list[i]);
                        }
                    }
                }
            }

            for (var i=0; i<toRemove.length; i++) {
                var idx = list.indexOf(toRemove[i]);
                list.splice(idx, 1);
            }

            list.sort(function (a, b) {
                a = a.name.toUpperCase();
                b = b.name.toUpperCase();

                return (a < b) ? -1 : (a > b) ? 1 : 0;
            });

            vm.newVenueTimetable.venue = list[0].code;

            vm.venueList = list;
        };

        function getProgrammeTimetables() {
            timetableService.getAllTimetables('programme', vm.selectedYear,
                    vm.selectedSemester)
                .then(function(data) {
                    vm.programmeTimetables = data;

                    angular.forEach(vm.programmeTimetables, function(timetable) {
                        timetable.date = timetableService.setDisplayDate(timetable.date_updated);
                        timetable.isPublic = String(timetable.is_public);
                    });
                }
            );
        }

        function getVenueTimetables() {
            timetableService.getAllTimetables('venue', vm.selectedYear,
                    vm.selectedSemester)
                .then(function(data) {
                    vm.venueTimetables = data;

                    angular.forEach(vm.venueTimetables, function(timetable) {
                        timetable.date = timetableService.setDisplayDate(timetable.date_updated);
                        timetable.isPublic = String(timetable.is_public);

                        vm.setVenueList();
                    });
                }
            );
        }

        function success(message) {
            vm.error = null;
            vm.success = message;
        }

        function error(message) {
            vm.success = null;
            vm.error = message;
        }
    };
})();
