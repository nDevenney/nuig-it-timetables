(function() {
	'use strict';

	angular
		.module('timetablesApp.timetableManager', [
			'timetablesApp.activeSemester',
			'timetablesApp.timetable',
			'timetablesApp.programme',
			'timetablesApp.venue'
		]);
})();