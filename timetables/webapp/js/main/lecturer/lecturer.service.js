(function() {
    'use strict';
    
    angular
        .module('timetablesApp.lecturer')
        .factory('lecturerService', lecturerService);

    lecturerService.$inject = ['$http'];

    function lecturerService($http) {
        return {
            getAllLecturers: function() {
            	var url = 'api/lecturer/';

            	return $http.get(url).then(function(response) {
            		return response.data;
            	});
       		},

            getLecturer: function(email) {
                var url = 'api/lecturer/' + email + '/';

                return $http.get(url).then(function(response) {
                    return response.data;
                });
            },

            addLecturer: function(email, name) {
                var url = 'api/lecturer/';

                var request = {
                    method: 'POST',
                    url: url,
                    headers: {
                        'X-CSRFToken': Cookies.get('csrftoken')
                    },
                    data: {
                        'email': email,
                        'name': name
                    }
                };

                return $http(request)
                    .then(function(response) {
                        return response.data;
                    }
                );
            },

            getModuleAssignments: function() {
                var url = 'api/taughtby/';

                return $http.get(url).then(function(response) {
                    return response.data;
                });
            },

            addModuleAssignment: function(module, lecturer) {
                var url = 'api/taughtby/';

                var request = {
                    method: 'POST',
                    url: url,
                    headers: {
                        'X-CSRFToken': Cookies.get('csrftoken')
                    },
                    data: {
                        'module': module,
                        'lecturer': lecturer
                    }
                };

                return $http(request)
                    .then(function(response) {
                        return response.data;
                    }
                );
            },

            removeLecturerAssignment: function(id) {
                var url = 'api/taughtby/' + id + '/';

                var request = {
                    method: 'DELETE',
                    url: url,
                    headers: {
                        'X-CSRFToken': Cookies.get('csrftoken')
                    }
                };

                return $http(request)
                    .then(function(response) {
                        return response.data;
                    }
                );
            },

            updateLecturer: function(email, newEmail, name) {
                var url = 'api/lecturer/' + email + '/';

                var request = {
                    method: 'PUT',
                    url: url,
                    headers: {
                        'X-CSRFToken': Cookies.get('csrftoken')
                    },
                    data: {
                        'oldEmail': email,
                        'newEmail': newEmail,
                        'name': name
                    }
                };

                return $http(request)
                    .then(function(response) {
                        return response.data;
                    }
                );
            },

            deleteLecturer: function(email) {
                var url = 'api/lecturer/' + email + '/';

                var request = {
                    method: 'DELETE',
                    url: url,
                    headers: {
                        'X-CSRFToken': Cookies.get('csrftoken')
                    }
                };

                return $http(request)
                    .then(function(response) {
                        return response.data;
                    }
                );
            }
    	}
    }
})();