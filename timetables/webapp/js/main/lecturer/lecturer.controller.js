(function() {
    'use strict';

    angular
        .module('timetablesApp.lecturer')
        .controller('LecturerController', LecturerController);

    LecturerController.$inject = ['$scope', 'lecturerService'];

    function LecturerController($scope, lecturerService) {
        var vm = this;

        vm.newLecturerEmail = '';
        vm.newLecturerName = '';
        vm.success = null;
        vm.error = null;

        lecturerService.getAllLecturers()
            .then(function(data) {
                vm.lecturers = data;

                for (var i in vm.lecturers) {
                    vm.lecturers[i].newEmail = vm.lecturers[i].email;
                }
            }
        );

        lecturerService.getModuleAssignments()
            .then(function(data) {
                vm.moduleAssignments = data;
            }
        );

        vm.importLecturers = function() {
            var rows = vm.csv.split('\n');
            var lecturers = [];
            var assignments = [];

            // build lecturer objects
            for (var i=1; i<rows.length; i++) {
                if (!rows[i].includes('@')) {
                    continue;
                }

                var columns = rows[i].split(',');
                var lecturer = {};
                var assignment = {};

                // get name and email
                lecturer.name = columns[0];
                lecturer.email = columns[1];
                lecturer.newEmail = columns[1];

                var alreadyExists = false;

                angular.forEach(lecturers, function(l) {
                    if (l.email == lecturer.email) {
                        alreadyExists = true;
                    }
                });

                if (!alreadyExists) {
                    lecturers.push(lecturer);
                }

                // get assignment
                assignment.lecturer = lecturer.email;
                assignment.module = columns[2].trim();

                assignments.push(assignment);
            }

            storeLecturers(lecturers, assignments);
        };

        vm.updateLecturer = function(lecturer) {
            lecturerService.updateLecturer(lecturer.email, lecturer.newEmail, lecturer.name)
                .then(function(data) {
                    success('Lecturer ' + lecturer.name + ' successfully updated');

                    var idx = vm.lecturers.indexOf(lecturer);
                    vm.lecturers[idx].email = data.email;
                    vm.lecturers[idx].newEmail = data.email;
                    vm.lecturers[idx].name = data.name;
                }, function() {
                    error('Error updating lecturer (did you use a valid email?)');
                }
            )
        };

        vm.deleteLecturer = function(lecturer) {
            lecturerService.deleteLecturer(lecturer.email)
                .then(function(data) {
                    success('Lecturer ' + lecturer.name + ' successfully deleted');

                    var idx = vm.lecturers.indexOf(lecturer);
                    vm.lecturers.splice(idx, 1);
                }, function() {
                    //error('Error deleting lecturer (the server might be down)');
                }
            );
        };

        function storeLecturers(lecturers, assignments) {
            var num = 0;

            angular.forEach(lecturers, function(lecturer) {
                var alreadyExists = false;

                angular.forEach(vm.lecturers, function(l) {
                    if (l.email == lecturer.email) {
                        alreadyExists = true;
                    }
                });

                if (!alreadyExists) {
                    lecturerService.addLecturer(lecturer.email, lecturer.name)
                        .then(function(data) {
                            if (vm.error == null) {
                                success('Lecturer ' + lecturer.name + ' successfully added');
                            }

                            vm.lecturers.push({
                                'email': lecturer.email,
                                'newEmail': lecturer.email,
                                'name': lecturer.name
                            });
                        }, function() {
                            //error('Error adding lecturer (did you use a valid email?)');
                        }
                    );
                }

                num++;

                if (num === lecturers.length) {
                    storeAssignments(assignments);
                }
            });
        }

        function storeAssignments(assignments) {
            angular.forEach(assignments, function(assignment) {
                var alreadyExists = false;

                angular.forEach(vm.moduleAssignments, function(a) {
                    if (a.module === assignment.module &&
                            a.lecturer.email === assignment.lecturer) {
                        alreadyExists = true;
                    }
                });

                if (!alreadyExists) {
                    lecturerService.addModuleAssignment(assignment.module, assignment.lecturer)
                        .then(function(data) {
                            vm.moduleAssignments.push({
                                'lecturer': {
                                    'email': assignment.lecturer
                                },
                                'module': assignment.module
                            });
                        }, function() {
                            //error('Error creating module assignment');
                        }
                    );
                }
            });     
        }

        function success(message) {
            vm.error = null;
            vm.success = message;
        }

        function error(message) {
            vm.success = null;
            vm.error = message;
        }

        // csv file reading
        var fileInput = document.getElementById("csv");

        fileInput.addEventListener('change', readFile);

        function readFile() {
            var reader = new FileReader();
            reader.onload = function () {
                vm.csv = reader.result;
                $scope.$apply();
            };

            reader.readAsBinaryString(fileInput.files[0]);
        }
    };
})();
