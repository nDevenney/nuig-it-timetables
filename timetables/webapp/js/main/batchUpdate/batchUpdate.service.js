(function() {
    'use strict';
    
    angular
        .module('timetablesApp.batchUpdate')
        .factory('batchUpdateService', batchUpdateService);

    batchUpdateService.$inject = ['$http'];

    function batchUpdateService($http) {
        return {
            updateProgramme: function(year, code) {
            	var url = 'api/batchupdate/' + year + '/' + code + '/';

            	return $http.get(url).then(function(response) {
            		return response.data;
            	});
       		}
    	}
    }
})();