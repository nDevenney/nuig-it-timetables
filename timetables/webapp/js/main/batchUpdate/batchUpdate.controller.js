(function() {
    'use strict';

    angular
        .module('timetablesApp.batchUpdate')
        .controller('BatchUpdateController', BatchUpdateController);

    BatchUpdateController.$inject = ['$scope', 'batchUpdateService', 
            'programmeService'];

    function BatchUpdateController($scope, batchUpdateService, programmeService) {
        var vm = this;

        vm.currentYear = new Date().getFullYear();
        vm.selectedYear = String(vm.currentYear);
        vm.programmes = [];
        vm.newProgrammeStatus = 'idle';

        // populate 'years' dropdown
        vm.years = [{
            'name': vm.currentYear + '/' + (vm.currentYear + 1),
            'id': String(vm.currentYear)
        }];

        for (var i=1; i<5; i++) {
            vm.years.push({
                'name': (vm.currentYear - i) + '/' + (vm.currentYear - i + 1),
                'id': String(vm.currentYear - i)
            });
        }

        // find existing programme data
        programmeService.getAllProgrammes()
            .then(function(data) {
                vm.programmes = data;

                vm.initProgrammes();
            }
        );

        vm.initProgrammes = function() {
            for(var i in vm.programmes) {
                vm.programmes[i].updateStatus = 'idle';
            }
        }

        vm.updateProgramme = function(programme) {
            programme.updateStatus = "updating"

            batchUpdateService.updateProgramme(vm.selectedYear, programme.code)
                .then(function(data) {
                    programme.updateStatus = data.status;
            });        
        };

        vm.updateAllProgrammes = function() {
            for(var i in vm.programmes) {
                vm.updateProgramme(vm.programmes[i]);
            }
        };

        vm.addNewProgramme = function(code) {
            for(var i in vm.programmes) {
                if (vm.programmes[i].code === vm.newProgramme) {
                    vm.newProgrammeStatus = 'Programme already exists';
                    return;
                }
            }

            vm.newProgrammeStatus = 'updating';

            batchUpdateService.updateProgramme(vm.selectedYear, code)
                .then(function(data) {
                    if (data.status === 'Success') {
                        programmeService.getProgramme(code)
                            .then(function(data) {
                                vm.newProgrammeStatus = 'Success';
                                data.updateStatus = 'idle';
                                vm.programmes.push(data);
                        });
                    } else {
                        vm.newProgrammeStatus = data.status;
                    }
            });        
        };

        vm.addProgrammeInputChanged = function() {
            if (vm.newProgrammeStatus !== 'updating') {
                vm.newProgrammeStatus = 'idle';
            }
        };

        vm.deleteProgramme = function(programme) {
            programmeService.deleteProgramme(programme.code)
                .then(function(data) {
                    var idx = vm.programmes.indexOf(programme);
                    vm.programmes.splice(idx, 1);
                }
            );
        };

        // ng-show logic too complex to be in the HTML
        vm.showProgrammeUpdateStatus = function(programme) {
            return programme.updateStatus !== 'idle' && 
                    programme.updateStatus !== 'updating';
        };

        vm.showAddNewProgrammeButton = function() {
            return vm.newProgramme && vm.newProgrammeStatus !== 'updating' &&
                    vm.newProgrammeStatus !== 'Success';
        };

        vm.showNewProgrammeStatus = function() {
            return vm.newProgrammeStatus !== 'idle' && 
                    vm.newProgrammeStatus !== 'updating';
        }
    };
})();
