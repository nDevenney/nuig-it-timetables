(function() {
	'use strict';

	angular
		.module('timetablesApp.batchUpdate', [
			'timetablesApp.programme'
		]);
})();