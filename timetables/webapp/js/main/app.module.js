(function() {
    'use strict';

    angular
        .module('timetablesApp', [
            'ui.router',
            'timetablesApp.batchUpdate',
            'timetablesApp.lecturer',
            'timetablesApp.module',
            'timetablesApp.moduleDetail',
            'timetablesApp.programme',
            'timetablesApp.timetable',
            'timetablesApp.venue',
            'timetablesApp.timetablePdf',
            'timetablesApp.activeSemester',
            'timetablesApp.timetableManager',
            'timetablesApp.timetableEntry'
        ])
        .config(["$stateProvider", "$urlRouterProvider", "$locationProvider", function($stateProvider, $urlRouterProvider, $locationProvider) {
            $stateProvider
                .state('home', {
                    url: '/app',
                    templateUrl: '/webapp/templates/home.html'
                })
                .state('batchUpdate', {
                    url: '/app/admin/batchupdate',
                    templateUrl: '/webapp/templates/batch-update.html',
                    controller: 'BatchUpdateController',
                    controllerAs: 'vm'
                })
                .state('activeSemester', {
                    url: '/app/admin/activesemester',
                    templateUrl: '/webapp/templates/active-semester.html',
                    controller: 'ActiveSemesterController',
                    controllerAs: 'vm'
                })
                .state('manageLecturers', {
                    url: '/app/admin/lecturers',
                    templateUrl: '/webapp/templates/manage-lecturers.html',
                    controller: 'LecturerController',
                    controllerAs: 'vm'
                })
                .state('manageVenues', {
                    url: '/app/admin/venues',
                    templateUrl: '/webapp/templates/manage-venues.html',
                    controller: 'VenueController',
                    controllerAs: 'vm'
                })
                .state('moduleList', {
                    url: '/app/admin/module',
                    templateUrl: '/webapp/templates/module-list.html',
                    controller: 'ModuleController',
                    controllerAs: 'vm'
                })
                .state('moduleDetail', {
                    url: '/app/admin/module/{moduleCode}',
                    templateUrl: '/webapp/templates/module-detail.html',
                    controller: 'ModuleDetailController',
                    controllerAs: 'vm'
                })
                .state('timetableSpecific', {
                    url: '/app/timetables/{type}/{code}',
                    templateUrl: 'webapp/templates/timetable.html',
                    controller: 'TimetableController',
                    controllerAs: 'vm',
                    resolve: {
                        activeSemester: ["$http", function($http) {
                            return $http.get('api/activesemester/')
                                .then(function(response) {
                                    return response.data;
                                }
                            );
                        }]
                    }
                })    
                .state('timetable', {
                    url: '/app/timetables/{type}',
                    templateUrl: 'webapp/templates/timetable.html',
                    controller: 'TimetableController',
                    controllerAs: 'vm',
                    resolve: {
                        activeSemester: ["$http", function($http) {
                            return $http.get('api/activesemester/')
                                .then(function(response) {
                                    return response.data;
                                }
                            );
                        }]
                    }
                })
                .state('manageTimetables', {
                    url: '/app/admin/timetables',
                    templateUrl: 'webapp/templates/manage-timetables.html',
                    controller: 'TimetableManagerController',
                    controllerAs: 'vm'
                });

            $urlRouterProvider
                .otherwise('/app/timetables/programme');

            $locationProvider.html5Mode(true);
        }]);
})();

