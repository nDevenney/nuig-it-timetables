(function() {
    'use strict';
    
    angular
        .module('timetablesApp.programme')
        .factory('programmeService', programmeService);

    programmeService.$inject = ['$http'];

    function programmeService($http) {
        return {
            getAllProgrammes: function() {
            	var url = 'api/programme/';

            	return $http.get(url).then(function(response) {
            		return response.data;
            	});
       		},

            getProgramme: function(code) {
                var url = 'api/programme/' + code + '/';

                return $http.get(url).then(function(response) {
                    return response.data;
                });
            },

            getProgrammeModules: function(code) {
                var url = 'api/programme/' + code + '/modules/';

                return $http.get(url).then(function(response) {
                    return response.data;
                });
            },

            deleteProgramme: function(code) {
                var url = 'api/programme/' + code + '/';

                var request = {
                    method: 'DELETE',
                    url: url,
                    headers: {
                        'X-CSRFToken': Cookies.get('csrftoken')
                    }
                };

                return $http(request)
                    .then(function(response) {
                        return response.data;
                    }
                );
            }
    	}
    }
})();