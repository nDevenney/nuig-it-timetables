(function() {
    'use strict';

    angular
        .module('timetablesApp.module')
        .controller('ModuleController', ModuleController);

    ModuleController.$inject = ['$scope', 'moduleService', 'programmeService'];

    function ModuleController($scope, moduleService, programmeService) {
        var vm = this;

        // pagination
        vm.currentPage = 1;
        vm.pageContent = [];
        vm.filteredModules = [];

        // cohort filter
        vm.filter = false;
        vm.cohorts = [];

        // populate up some modules
        moduleService.getAllModules()
            .then(function(data) {
                vm.modules = data;

                // add cohorts
                angular.forEach(vm.modules, function(module) {
                    if (module.taken_by[0]) {
                        var cohorts = module.taken_by[0].programme.code;

                        for (var i=1; i<module.taken_by.length; i++) {
                            cohorts += ', ' + module.taken_by[i].programme.code;
                        }

                        module.cohorts = cohorts;
                    }
                });

                // filter off by default
                vm.filteredModules = vm.modules;

                // populate first page for pagination
                for (var i=0; i<10; i++) {
                    vm.pageContent.push(vm.filteredModules[i]);
                }
            }
        );

        // now get the programmes for the cohort filter
        programmeService.getAllProgrammes()
            .then(function(data) {
                angular.forEach(data, function(programme) {
                    vm.cohorts.push(programme.code);
                });

                // set filter programme as the first one returned
                vm.filterProgramme = vm.cohorts[0];
            }
        );

        vm.pageChanged = function() {
            // update page content
            var startIdx = (vm.currentPage - 1) * 10;

            vm.pageContent = [];

            for (var i=startIdx; i<(startIdx + 10); i++) {
                if (i == vm.filteredModules.length) {
                    break;
                }

                vm.pageContent.push(vm.filteredModules[i]);
            }
        };

        vm.filterModules = function() {
            if (!vm.filter) {
                // do everything as normal
                vm.filteredModules = vm.modules;

                // update page
                vm.currentPage = 1;
                vm.pageChanged();
            } else {
                // filter modules by cohort
                vm.filteredModules = [];

                // string matching for cohorts
                angular.forEach(vm.modules, function(module) {
                    if (module.cohorts.indexOf(vm.filterProgramme) > -1) {
                        vm.filteredModules.push(module);
                    }
                });

                // update page
                vm.currentPage = 1;
                vm.pageChanged();
            }
        };
    };
})();
