(function() {
	'use strict';

	angular
		.module('timetablesApp.module', [
			'ui.bootstrap',
			'timetablesApp.programme'
		]);
})();