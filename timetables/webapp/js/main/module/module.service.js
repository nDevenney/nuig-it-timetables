(function() {
    'use strict';
    
    angular
        .module('timetablesApp.module')
        .factory('moduleService', moduleService);

    moduleService.$inject = ['$http'];

    function moduleService($http) {
        return {
            getAllModules: function() {
            	var url = 'api/module/';

            	return $http.get(url).then(function(response) {
            		return response.data;
            	});
       		},

            getModule: function(code) {
                var url = 'api/module/' + code + '/';

                return $http.get(url).then(function(response) {
                    return response.data;
                });
            },

            getProgrammesTakingModule: function(code) {
                var url = 'api/module/' + code + '/takenby/';

                return $http.get(url).then(function(response) {
                    return response.data;
                });
            },

            getModuleTimetableEntries: function(code) {
                var url = 'api/module/' + code + '/entries/';

                return $http.get(url).then(function(response) {
                    return response.data;
                });
            },

            updateShortName: function(code, shortName) {
                var url = 'api/module/' + code + '/';

                var request = {
                    method: 'PUT',
                    url: url,
                    headers: {
                        'X-CSRFToken': Cookies.get('csrftoken')
                    },
                    data: {
                        'code': code,
                        'shortName': shortName
                    }
                };

                return $http(request)
                    .then(function(response) {
                        return response.data;
                    }
                );
            },
    	}
    }
})();