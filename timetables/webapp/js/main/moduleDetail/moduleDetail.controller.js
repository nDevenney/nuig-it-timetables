(function() {
    'use strict';

    angular
        .module('timetablesApp.moduleDetail')
        .controller('ModuleDetailController', ModuleDetailController);

    ModuleDetailController.$inject = ['$scope', '$stateParams', 'moduleService',
        'activeSemesterService', 'timetableEntryService', 'timetableService',
        'lecturerService', 'venueService'];

    function ModuleDetailController($scope, $stateParams, moduleService,
            activeSemesterService, timetableEntryService, timetableService,
            lecturerService, venueService) {
        var vm = this;

        var allEntries; // every entry for the semester
        var cohortEntries; // every entry for this module's cohorts

        vm.editing = false;

        vm.days = [
            { id: 1, label: 'Monday'},
            { id: 2, label: 'Tuesday'},
            { id: 3, label: 'Wednesday'},
            { id: 4, label: 'Thursday'},
            { id: 5, label: 'Friday'}
        ];

        vm.selectedDay = 1;

        vm.times = [
            { id: 9, label: '9:00am', disabled: false},
            { id: 10, label: '10:00am', disabled: false},
            { id: 11, label: '11:00am', disabled: false},
            { id: 12, label: '12:00pm', disabled: false},
            { id: 13, label: '1:00pm', disabled: false},
            { id: 14, label: '2:00pm', disabled: false},
            { id: 15, label: '3:00pm', disabled: false},
            { id: 16, label: '4:00pm', disabled: false},
            { id: 17, label: '5:00pm', disabled: false},
            { id: 18, label: '6:00pm', disabled: false},
            { id: 19, label: '7:00pm', disabled: false},
        ];

        vm.selectedTime = 9;

        vm.types = [
            { id: false, label: 'Lecture'},
            { id: true, label: 'Lab'}
        ];

        vm.isLabEntry = false;
        vm.selectedSpecificProgramme = "All";

        vm.selectedLecturers = [];
        vm.selectedVenues = [];

        // get information on module including lecturers and cohorts
        moduleService.getModule($stateParams.moduleCode)
            .then(function(data) {
                vm.module = data;

                activeSemesterService.getActiveSemester()
                    .then(function(data) {
                        vm.activeSemester = data;

                        getModuleEntries();
                        allEntries = getAllEntries();
                    }
                );

                if (vm.module.lecturers.length == 1) {
                    vm.selectedLecturers.push(vm.module.lecturers[0].lecturer.email);
                }

                // module can have split entries if at least one cohort has
                // this as an optional module
                vm.module.canBeSplit = false;

                for (var i=0; i<vm.module.taken_by.length; i++) {
                    if (!vm.module.taken_by[i].is_core) {
                        vm.module.canBeSplit = true;
                        break;
                    }
                }
            }
        );

        // get venues
        venueService.getAllVenues()
            .then(function(data) {
                vm.venues = [];

                angular.forEach(data, function(venue) {
                    vm.venues.push({
                        'id': venue.code,
                        'label': venue.name,
                        'disabled': false
                    });
                });
            }
        );

        vm.addEntry = function() {
            timetableEntryService.addEntry(vm.selectedDay, vm.selectedTime,
                    vm.activeSemester.semester, vm.activeSemester.year,
                    vm.isLabEntry, $stateParams.moduleCode,
                    vm.selectedLecturers, vm.selectedVenues,
                    vm.selectedSpecificProgramme)
                .then(function(data) {
                    if (vm.editing) {
                        vm.editing = false;
                    }

                    allEntries = getAllEntries();

                    getModuleEntries();
                }
            );
        };

        vm.edit = function(entry) {
            if (vm.editing) {
                return;
            }

            vm.editing = true;

            timetableEntryService.deleteEntry(entry.id)
                .then(function(data) {
                    var idx = vm.entries.indexOf(entry);
                    vm.entries.splice(idx, 1);

                    allEntries = getAllEntries();

                    vm.selectedDay = entry.day;
                    vm.selectedTime = entry.time;
                    vm.selectedLecturers = [];

                    for (var i=0; i<entry.lecturers.length; i++) {
                        vm.selectedLecturers.push(entry.lecturers[i].email);
                    }

                    vm.isLabEntry = entry.is_lab_entry;
                    vm.selectedVenues = [];

                    for (var i=0; i<entry.venues.length; i++) {
                        vm.selectedVenues.push(entry.venues[i].code);
                    }

                    vm.selectedSpecificProgramme = "All";
                }
            );

        };

        vm.removeLecturerAssignment = function(lecturer) {
            lecturerService.removeLecturerAssignment(lecturer.id)
                .then(function(data) {
                    var idx = vm.module.lecturers.indexOf(lecturer);
                    vm.module.lecturers.splice(idx, 1);
                }
            );
        };

        vm.findSuitableTimes = function() {
            vm.splitWarning = false;

            angular.forEach(vm.times, function(time) {
                time.disabled = false;
            });

            splitWarning();

            // if only one lecturer, grey out times they aren't available
            if (vm.module.lecturers.length === 1) {
                for(var i=0; i<allEntries.length; i++) {
                    if (allEntries[i].day === vm.selectedDay) {
                        for (var j=0; j<allEntries[i].lecturers.length; j++) {
                            if (allEntries[i].lecturers[j].email === vm.module.lecturers[0].lecturer.email) {
                                for (var k=0; k<vm.times.length; k++) {
                                    if (vm.times[k].id === allEntries[i].time) {
                                        if (!allEntries[i].is_lab_entry) {
                                            //if (!vm.isLabEntry) {
                                                vm.times[k].disabled = true;
                                            //}
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            checkSelectedTimeDisabled();

            vm.findSuitableLecturersAndVenues();
            
        };

        function checkSelectedTimeDisabled() {
            for (var i=0; i<vm.times.length; i++) {
                if (vm.selectedTime === vm.times[i].id &&
                        vm.times[i].disabled) {
                    for (var j=0; j<vm.times.length; j++) {
                        if (!vm.times[j].disabled) {
                            vm.selectedTime = vm.times[j].id;
                            break;
                        }
                    }

                    break;
                }
            }
        }

        vm.findSuitableLecturersAndVenues = function() {
            vm.splitWarning = false;

            angular.forEach(vm.module.lecturers, function(lecturer) {
                lecturer.disabled = false;
            });

            angular.forEach(vm.venues, function(venue) {
                venue.disabled = false;
            });

            // for modules with multiple lecturers
            angular.forEach(vm.module.lecturers, function(lecturer) {
                angular.forEach(allEntries, function(entry) {
                    if (entry.time === vm.selectedTime &&
                            entry.day === vm.selectedDay) {
                        angular.forEach(entry.lecturers, function(entryLecturer) {
                            if (entryLecturer.email === lecturer.lecturer.email) {
                                lecturer.disabled = true;
                            }
                        });                          
                    }          
                });
            });

            angular.forEach(vm.venues, function(venue) {
                angular.forEach(allEntries, function(entry) {
                    if (entry.time === vm.selectedTime &&
                            entry.day === vm.selectedDay) {
                        angular.forEach(entry.venues, function(entryVenue) {
                            if (entryVenue.code === venue.id) {
                                venue.disabled = true;
                            }
                        });                          
                    }          
                });
            });

            splitWarning();
        };

        vm.updateShortName = function() {
            moduleService
                .updateShortName($stateParams.moduleCode, vm.module.short_name)
                .then(function(data) {
                    vm.shortNameChangeSuccess = true;
                }
            );
        };

        vm.shortNameChanged = function() {
            vm.shortNameChangeSuccess = false;
        };

        vm.deleteEntry = function(entry) {
            timetableEntryService.deleteEntry(entry.id)
                .then(function(data) {
                    var idx = vm.entries.indexOf(entry);
                    vm.entries.splice(idx, 1);

                    allEntries = getAllEntries();
                }
            );
        };

        vm.disableSubmit = function() {
            return false; // for now
        }

        function splitWarning() {            
            vm.warningMessage = '';

            angular.forEach(vm.times, function(time) {
                angular.forEach(cohortEntries, function(entry) {
                    if (entry.day === vm.selectedDay &&
                            entry.time === time.id) {
                        if (!vm.module.canBeSplit || entry.module.code === $stateParams.moduleCode) {
                            time.disabled = true;
                        } else {
                            var canBeSplit = true;

                            for (var i=0; i<entry.module.taken_by.length; i++) {
                                if (entry.module.taken_by[i].is_core) {
                                    canBeSplit = false;
                                    break;
                                }
                            }

                            if (!canBeSplit) {
                                time.disabled = true;
                            } else {
                                // need to warn admin
                                if (time.id === vm.selectedTime) {
                                    vm.splitWarning = true;
                                    vm.warningMessage = "Another module is scheduled in this timeslot (";
                                    vm.warningMessage += entry.module.code + "- " + entry.module.name + ')\n';
                                }
                            }
                        }
                    }

                });
            });

        }

        function getModuleEntries() {
            moduleService.getModuleTimetableEntries($stateParams.moduleCode)
                .then(function(data) {
                    vm.entries = [];

                    // only get entries for this active semester
                    angular.forEach(data, function(entry) {
                        if (entry.year === vm.activeSemester.year &&
                                entry.semester === vm.activeSemester.semester) {
                            vm.entries.push(entry);
                        }
                    });

                    // string representation of lecturers
                    angular.forEach(vm.entries, function(entry) {
                        if (entry.lecturers.length < 1) {
                            return;
                        }

                        var lecturersDisplay = entry.lecturers[0].name;

                        for (var i=1; i<entry.lecturers.length; i++) {
                            lecturersDisplay += ', ' + 
                                entry.lecturers[i].name;
                        }

                        entry.lecturersDisplay = lecturersDisplay;
                    });

                    // string representation of venues
                    angular.forEach(vm.entries, function(entry) {
                        if (entry.venues.length < 1) {
                            return;
                        }

                        var venuesDisplay = entry.venues[0].name;

                        for (var i=1; i<entry.venues.length; i++) {
                            venuesDisplay += ', ' + 
                                entry.venues[i].name;
                        }

                        entry.venuesDisplay = venuesDisplay;
                    });
                }
            );
        }

        function getAllEntries() {
            var allEntries = [];
            cohortEntries = [];

            timetableService.getAllTimetables('programme',
                    vm.activeSemester.year, vm.activeSemester.semester)
                .then(function(data) {
                    angular.forEach(data, function(timetable) {
                        allEntries.push.apply(allEntries, timetable.entries);

                        // check for cohorts
                        angular.forEach(vm.module.taken_by, function(cohort) {
                            if (timetable.programme.code === cohort.programme.code) {
                                cohortEntries.push.apply(cohortEntries,
                                    timetable.entries);
                            }
                        });
                    });

                    vm.findSuitableTimes();
                }
            );

            return allEntries;         
        }
    };
})();
