(function() {
	'use strict';

	angular
		.module('timetablesApp.moduleDetail', [
			'timetablesApp.module',
			'timetablesApp.activeSemester',
			'timetablesApp.timetableEntry',
			'timetablesApp.timetable',
			'timetablesApp.lecturer',
			'timetablesApp.venue'
		]);
})();