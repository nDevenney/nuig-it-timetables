(function() {
    'use strict';
    
    angular
        .module('timetablesApp.timetablePdf')
        .factory('timetablePdfService', timetablePdfService);

    function timetablePdfService() {
        return {
        	generatePdf: function(days, times, type, year, semester,
        			timetableDisplay, activeTimetable, date) {
            	// create the heading sub table body
                var heading = [];

                heading.push({
                    text: activeTimetable[type].code,
                    colSpan: 2, 
                    alignment: 'left',
                    fontSize: 20,
                    bold: true,
                    margin: 10
                });

                heading.push({});

                if (type === 'programme') {
                    heading.push({
                        text: 'Timetable ' + year,
                        colSpan: 2,
                        alignment: 'center',
                        fontSize: 20,
                        bold: true,
                        margin: 10
                    });
                } else {
                    heading.push({
                        text: 'Lab Timetable ' + year,
                        colSpan: 2,
                        alignment: 'center',
                        fontSize: 20,
                        bold: true,
                        margin: 10
                    });
                }

                heading.push({});

                heading.push({
                    text: 'Semester ' + semester,
                    colSpan: 2,
                    alignment: 'right',
                    fontSize: 20,
                    bold: true,
                    margin: 10
                });
                
                heading.push({});

                // create the footer subtable
                var footer = [];

                footer.push({
                    text: "Version " + activeTimetable.version,
                    colSpan: 2,
                    alignment: 'left',
                    margin: 0
                })

                footer.push({});

                footer.push({
                    text: "http://www.it.nuigalway.ie/",
                    colSpan: 2,
                    alignment: 'center',
                    margin: 0
                })

                footer.push({});

                footer.push({
                    text: date,
                    colSpan: 2,
                    alignment: 'right',
                    margin: 0
                })

                footer.push({});

                // create the days sub heading rows
                var displayDays = [];

                displayDays.push({
                    text: 'Time/Day',
                    style: 'tableHeader',
                    alignment: 'center',
                    fontSize: 13,
                    bold: true,
                    margin: 3
                });

                for (var i in days) {
                    displayDays.push({
                        text: days[i],
                        style: 'tableHeader',
                        alignment: 'center',
                        fontSize: 13,
                        bold: true,
                        margin: 3
                    });
                }

                // create the timetable entry rows
                var entries = [];

                for (var i in times) {
                    var row = [];

                    row.push({
                        text: '\n' + times[i],
                        alignment: 'center',
                        fontSize: 13,
                        bold: true,
                        margin: [0, 5, 0, 0]
                    });

                    for (var j in timetableDisplay[i]) {
                        var entry = timetableDisplay[i][j];
                        var text;
                        var margin;
                        var name;

                        if (entry === null) {
                            text = '';
                            margin = 30;
                        } else if (!entry.isSplit && type === 'programme') {
                            name = entry.code + ' ' + entry.name;
                            text = name + '\n';
                            text += entry.venues + '\n';
                            text += entry.lecturers;

                            if (name.length < 21) {
                                margin = [0, 7, 0, 7];
                            } else {
                                margin = 0
                            }
                        } else if (type === 'venue') {
                            name = entry.code + ' ' + entry.name;
                            text = entry.code + ' ' + entry.name + '\n';
                            text += entry.lecturers + '\n';

                            if (!entry.specificToProgramme === null) {
                                text += entry.specificToProgramme;
                            }
                            
                            if (name.length < 21) {
                                margin = [0, 7, 0, 7];
                            } else {
                                margin = 0
                            }
                        } else if (entry.isSplit) {
                            text = '[A] ' + entry['A'].code + ' ' + entry['A'].name + ' ' +  entry['A'].venues + '\n';
                            text += '[B] ' + entry['B'].code + ' ' + entry['B'].name + ' ' +  entry['B'].venues;
                            margin = 0;
                        }

                        row.push({
                            text: text,
                            alignment: 'center',
                            margin: margin
                        });             
                    }

                    entries.push(row);
                }

                // create the body of the actual rendered timetable
                var body = [
                    [{
                        table: {
                            widths: [ 76, 136, 136, 136, 136, 136 ],
                            body: [
                                heading
                            ],      
                        },
                        layout: 'noBorders',
                        colSpan: 6
                    }, {}, {}, {}, {}, {}],
                    displayDays
                ];

                for (var i in entries) {
                    body.push(entries[i]);
                }

                var docDefinition = { 
                    pageOrientation: 'landscape',
                    pageSize: {
                        'width': 900,
                        'height': 900
                    },
                    content: [
                        {
                            style: 'tableExample',
                            color: '#444',
                            table: {
                                widths: [ 76, 136, 136, 136, 136, 136 ],
                                body: body
                            }
                        },
                        {
                            style: 'tableExample',
                            color: '#444',
                            table: {
                                widths: [ 76, 136, 136, 136, 136, 136 ],
                                body: [footer]
                            },
                            layout: 'noBorders'
                        }
                    ]
                };

                return docDefinition;
        	}
        }
    }
})();