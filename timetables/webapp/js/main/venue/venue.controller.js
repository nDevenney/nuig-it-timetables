(function() {
    'use strict';

    angular
        .module('timetablesApp.venue')
        .controller('VenueController', VenueController);

    VenueController.$inject = ['$scope', 'venueService'];

    function VenueController($scope, venueService) {
        var vm = this;

        vm.newVenueCode = '';
        vm.newVenueName = '';
        vm.success = null;
        vm.error = null;

        venueService.getAllVenues()
            .then(function(data) {
                vm.venues = data;

                for (var i in vm.venues) {
                    vm.venues[i].newCode = vm.venues[i].code;
                }
            }
        );

        vm.addVenue = function() {
            if (vm.newVenueCode === '' && vm.newVenueName === '') {
                return;
            }

            if (vm.newVenueCode === '') {
                vm.newVenueCode = vm.newVenueName;
            }

            if (vm.newVenueName === '') {
                vm.newVenueName = vm.newVenueCode;
            }

            venueService.addVenue(vm.newVenueCode, vm.newVenueName)
                .then(function(data) {
                    success('Venue ' + vm.newVenueName + ' successfully added');

                    vm.venues.push({
                        'code': vm.newVenueCode,
                        'newCode': vm.newVenueCode,
                        'name': vm.newVenueName
                    });

                    vm.newVenueName = '';
                    vm.newVenueCode = '';
                }, function() {
                    error('Error adding venue');

                    vm.newVenueName = '';
                    vm.newVenueCode = '';
                }
            );
        };

        vm.updateVenue = function(venue) {
            venueService.updateVenue(venue.code, venue.newCode, venue.name)
                .then(function(data) {
                    success('Venue ' + venue.name + ' successfully updated');

                    var idx = vm.venues.indexOf(venue);
                    vm.venues[idx].code = data.code;
                    vm.venues[idx].newCode = data.code;
                    vm.venues[idx].name = data.name;
                }, function() {
                    error('Error updating venue');
                }
            )
        };

        vm.deleteVenue = function(venue) {
            venueService.deleteVenue(venue.code)
                .then(function(data) {
                    success('Venue ' + venue.name + ' successfully deleted');

                    var idx = vm.venues.indexOf(venue);
                    vm.venues.splice(idx, 1);
                }, function() {
                    error('Error deleting venue');
                }
            );
        };

        function success(message) {
            vm.error = null;
            vm.success = message;
        }

        function error(message) {
            vm.success = null;
            vm.error = message;
        }
    };
})();
