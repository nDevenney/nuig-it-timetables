(function() {
    'use strict';

    angular
        .module('timetablesApp.venue')
        .factory('venueService', venueService);

    venueService.$inject = ['$http'];

    function venueService($http) {
        return {
            getAllVenues: function() {
            	var url = 'api/venue/';

            	return $http.get(url).then(function(response) {
            		return response.data;
            	});
       		},

            getVenue: function(code) {
                var url = 'api/venue/' + venue + '/';

                return $http.get(url).then(function(response) {
                    return response.data;
                });
            },

            addVenue: function(code, name) {
                var url = 'api/venue/';

                var request = {
                    method: 'POST',
                    url: url,
                    headers: {
                        'X-CSRFToken': Cookies.get('csrftoken')
                    },
                    data: {
                        'code': code,
                        'name': name
                    }
                };

                return $http(request)
                    .then(function(response) {
                        return response.data;
                    }
                );
            },

            updateVenue: function(code, newCode, name) {
                var url = 'api/venue/' + code + '/';

                var request = {
                    method: 'PUT',
                    url: url,
                    headers: {
                        'X-CSRFToken': Cookies.get('csrftoken')
                    },
                    data: {
                        'oldCode': code,
                        'newCode': newCode,
                        'name': name
                    }
                };

                return $http(request)
                    .then(function(response) {
                        return response.data;
                    }
                );
            },

            deleteVenue: function(code) {
                var url = 'api/venue/' + code + '/';

                var request = {
                    method: 'DELETE',
                    url: url,
                    headers: {
                        'X-CSRFToken': Cookies.get('csrftoken')
                    }
                };

                return $http(request)
                    .then(function(response) {
                        return response.data;
                    }
                );
            }
    	}
    }
})();