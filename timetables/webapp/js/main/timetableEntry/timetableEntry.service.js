(function() {
    'use strict';

    angular
        .module('timetablesApp.timetableEntry')
        .factory('timetableEntryService', timetableEntryService);

    timetableEntryService.$inject = ['$http'];

    function timetableEntryService($http) {
        return {
            addEntry: function(day, time, semester, year, isLabEntry, module,
                    lecturers, venues, specificToProgramme) {
                var url = 'api/entry/';

                var request = {
                    method: 'POST',
                    url: url,
                    headers: {
                        'X-CSRFToken': Cookies.get('csrftoken')
                    },
                    data: {
                        'day': day,
                        'time': time,
                        'semester': semester,
                        'year': year,
                        'isLabEntry': isLabEntry,
                        'module': module,
                        'lecturers': lecturers,
                        'venues': venues,
                        'specificToProgramme': specificToProgramme
                    }
                };

                return $http(request)
                    .then(function(response) {
                        return response.data;
                    }
                );
            },

            deleteEntry: function(id) {
                var url = 'api/entry/' + id + '/';

                var request = {
                    method: 'DELETE',
                    url: url,
                    headers: {
                        'X-CSRFToken': Cookies.get('csrftoken')
                    }
                };

                return $http(request)
                    .then(function(response) {
                        return response.data;
                    }
                );
            }
    	}
    }
})();