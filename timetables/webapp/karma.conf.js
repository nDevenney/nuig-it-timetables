module.exports = function(config){
    config.set({
        files : [
            'node_modules/angular/angular.js',
            'node_modules/angular-ui-router/release/angular-ui-router.js',
            'node_modules/angular-mocks/angular-mocks.js',
     
            'js/main/**/*.module.js',
            'js/main/**/*.service.js',
            'js/main/**/*.controller.js',
            'js/main/app.module.js',
     
            'js/test/mockData.js',
            'js/test/**/*.js'
        ],

        autoWatch : false,

        frameworks: ['jasmine'],  

        browsers : ['PhantomJS'],

        reporters: ['progress', 'coverage'], 

        preprocessors: {
            'js/main/**/*.js': ['coverage']
        },

        coverageReporter: {
            type: 'html',
            dir: 'coverage'
        },
        
        plugins : [
            'karma-jasmine',
            'karma-phantomjs-launcher',
            'karma-coverage'
        ]
})}