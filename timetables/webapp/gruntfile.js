module.exports = function(grunt) {
    grunt.initConfig({
        karma: {
            options: {
                configFile: 'karma.conf.js'
            },
            
            unit: {
                singleRun: true
            },

            continuous: {
                background: true
            }
        },
        watch: {
            karma: {
                files: ['js/main/**/*.js', 'js/test/**/*.js'],
                tasks: ['karma:continuous:run']
            }
        },
        concat: {
            js: { 
                src: ['js/main/**/*.module.js', 
                      'js/main/**/*.service.js', 
                      'js/main/**/*.controller.js'],
                dest: 'js/build/app.js'
            }
        },
        uglify: {
            js: { 
                src: ['js/build/app.js'],
                dest: 'js/build/app.js'
            }
        }       
    });

    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('unit-test', ['karma:continuous:start', 'watch:karma'])
    grunt.registerTask('build', ['concat', 'uglify'])
};