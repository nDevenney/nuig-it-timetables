from rest_framework import serializers
from timetables.models import *


class BatchUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = BatchUpdateStatus
        fields = ('status', 'timestamp',)


class ProgrammeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Programme
        fields = ('name', 'code',)


class LecturerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lecturer
        fields = ('email', 'name',)


class VenueSerializer(serializers.ModelSerializer):
    class Meta:
        model = Venue
        fields = ('code', 'name',)


class TakenBySerializer(serializers.ModelSerializer):
    programme = ProgrammeSerializer(read_only=True)
    module = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = TakenBy
        fields = ('module', 'programme', 'is_core',)


class TaughtBySerializer(serializers.ModelSerializer):
    module = serializers.PrimaryKeyRelatedField(read_only=True)
    lecturer = LecturerSerializer(read_only=True)

    class Meta:
        model = TakenBy
        fields = ('module', 'lecturer', 'id',)


class ModuleSerializer(serializers.ModelSerializer):
    taken_by = TakenBySerializer(read_only=True, many=True)
    lecturers = TaughtBySerializer(read_only=True, many=True)

    class Meta:
        model = Module
        fields = ('code', 'ects', 'name', 'short_name', 'semester', 'taken_by',
                  'lecturers',)


class TimetableEntrySerializer(serializers.ModelSerializer):
    module = ModuleSerializer(read_only=True)
    lecturers = LecturerSerializer(many=True)
    venues = VenueSerializer(many=True)
    specific_to_programme = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = TimetableEntry
        fields = ('id', 'day', 'time', 'semester', 'year', 'is_lab_entry',
                  'module', 'lecturers', 'venues', 'split',
                  'specific_to_programme',)


class ProgrammeTimetableSerializer(serializers.ModelSerializer):
    programme = ProgrammeSerializer(read_only=True)
    entries = TimetableEntrySerializer(read_only=True, many=True)

    class Meta:
        model = ProgrammeTimetable
        fields = ('year', 'semester', 'version', 'programme', 'date_updated',
                  'entries', 'is_public',)


class VenueTimetableSerializer(serializers.ModelSerializer):
    venue = VenueSerializer(read_only=True)
    entries = TimetableEntrySerializer(read_only=True, many=True)

    class Meta:
        model = VenueTimetable
        fields = ('year', 'semester', 'version', 'venue', 'date_updated',
                  'entries', 'is_public',)


class ActiveSemesterSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActiveSemester
        fields = ('semester', 'year',)
