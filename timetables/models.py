from django.db import models
from django.utils import timezone
from operator import attrgetter


class BatchUpdateStatus(models.Model):
    status = models.CharField(max_length=50)
    timestamp = models.DateTimeField(default=timezone.now())

    def __unicode__(self):
        return self.status


class Programme(models.Model):
    code = models.CharField(max_length=10, primary_key=True)
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.code


class Lecturer(models.Model):
    email = models.CharField(max_length=50, primary_key=True)
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name


class Module(models.Model):
    code = models.CharField(max_length=10, primary_key=True)
    name = models.CharField(max_length=50, default='')
    short_name = models.CharField(max_length=50, default='')
    semester = models.CharField(max_length=10, default='1')
    ects = models.IntegerField(default=0)

    def taken_by(self):
        return TakenBy.objects.filter(module=self)

    def lecturers(self):
        return TaughtBy.objects.filter(module=self)

    def __unicode__(self):
        return self.code


class Venue(models.Model):
    code = models.CharField(max_length=50, primary_key=True)
    name = models.CharField(max_length=50)


class Timetable(models.Model):
    year = models.IntegerField(default=0)
    semester = models.IntegerField(default=1)
    version = models.IntegerField(default=1)
    date_updated = models.DateTimeField(default=timezone.now())
    is_public = models.BooleanField(default=False)

    class Meta:
        abstract = True


class ProgrammeTimetable(Timetable):
    programme = models.ForeignKey('Programme')

    def entries(self):
        taken_by = TakenBy.objects.filter(programme=self.programme)
        modules = []

        for t in taken_by:
            modules.append(t.module)

        entries = []

        for m in modules:
            entries.extend(TimetableEntry.objects
                           .filter(module=m, semester=self.semester,
                                   year=self.year))

        entries.sort(key=attrgetter('day', 'time'))

        to_remove = []

        for e in entries:
            if (e.specific_to_programme is not None and
                    e.specific_to_programme != self.programme):
                to_remove.append(e)

        for e in entries:
            if (e.year != self.year or e.semester != self.semester):
                to_remove.append(e)

        for e in to_remove:
            entries.remove(e)

        return entries


class VenueTimetable(Timetable):
    venue = models.ForeignKey('Venue')

    def entries(self):
        entry_venues = EntryVenue.objects.filter(venue=self.venue)

        entries = []

        for e in entry_venues:
            try:
                entries.append(TimetableEntry.objects.get(id=e.entry.id))
            except TimetableEntry.DoesNotExist:
                pass  # need to fix this

        entries.sort(key=attrgetter('day', 'time'))

        to_remove = []

        for e in entries:
            if (e.year != self.year or e.semester != self.semester):
                to_remove.append(e)

        for e in to_remove:
            entries.remove(e)

        return entries


class TimetableEntry(models.Model):
    module = models.ForeignKey('Module')
    day = models.IntegerField(default=0)
    time = models.IntegerField(default=0)
    semester = models.IntegerField(default=1)
    year = models.IntegerField(default=0)
    is_lab_entry = models.BooleanField(default=False)
    split = models.CharField(default='', max_length=1)
    specific_to_programme = models.ForeignKey('Programme', null=True)

    def lecturers(self):
        entry_lecturers = EntryLecturer.objects.filter(entry=self.id)
        lecturers = []

        for e in entry_lecturers:
            lecturers.append(Lecturer.objects.get(email=e.lecturer.email))

        return lecturers

    def venues(self):
        entry_venues = EntryVenue.objects.filter(entry=self.id)
        venues = []

        for e in entry_venues:
            venues.append(Venue.objects.get(code=e.venue.code))

        return venues


class ActiveSemester(models.Model):
    year = models.IntegerField(default=0)
    semester = models.IntegerField(default=0)


# junction tables to model many-to-many relationships
class TakenBy(models.Model):
    programme = models.ForeignKey('Programme')
    module = models.ForeignKey('Module')
    is_core = models.BooleanField(default=False)


class TaughtBy(models.Model):
    module = models.ForeignKey('Module')
    lecturer = models.ForeignKey('Lecturer')


class EntryVenue(models.Model):
    entry = models.ForeignKey('TimetableEntry')
    venue = models.ForeignKey('Venue')


class EntryLecturer(models.Model):
    entry = models.ForeignKey('TimetableEntry')
    lecturer = models.ForeignKey('Lecturer')
