# Web-Based Timetable Entry, Validation and Display #

### Project set up ###

```
#!bash
git clone https://nDevenney@bitbucket.org/nDevenney/nuig-it-timetables.git

# create virtual python environment and activate it (optional, but good practice) 
virtualenv venv

source venv/bin/activate # linux
# ./venv/Scripts/activate  # windows 

# install python dependencies
cd nuig-it-timetables
pip install -r requirements.txt

# create database
./manage.py makemigrations
./manage.py makemigrations timetables
./manage.py migrate

# install javascript dependencies
cd timetables/webapp
npm install

# run server at localhost:8000
cd ../..
./manage.py runserver
```
