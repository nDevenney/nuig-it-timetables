# url routing for the entire project

from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),
    url(r'^change-password/$', auth_views.password_change, {'post_change_redirect': '/'}, name='password_change'),
    url(r'^admin/', admin.site.urls),
    url(r'^/?', include('timetables.urls')),
]
